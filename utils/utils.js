const bcrypt = require('bcrypt');
const saltRounds = 10;

module.exports.getHash = (string) => bcrypt.hash(string, saltRounds);

module.exports.compareHash = (string, hash) => bcrypt.compare(string, hash);

module.exports.regexDate = (date) => {
    const dateRegex = new RegExp(/^\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$/, 'i');
    return dateRegex.test(date);
    //date format : YYYY-MM-DD
}

module.exports.regexEmail = (email) => {
    const emailRegExp = new RegExp(/[\w.]+@\w+\.\w+/, 'i');
    return emailRegExp.test(email);
    // "Email must respect the following pattern : abc@example.foo"
}

module.exports.regexPassword = (password) => {
    const passwordRegexp = new RegExp(/((?=.*\d)(?=.*[a-z])(?=.*[A-Z])([-+_!@#$€%^&*.,?]*)){6,25}/, 'i');
    return passwordRegexp.test(password);
    // "Password must contain between 6 and 25 characters, including at least one letter in upper and in lower case. Special characters are recommended."
}

module.exports.regexName = (name) => {
    const nameRegexp = new RegExp(/[A-ZÇÁÀÂÄÉÈÊËÍÌÎÏÓÒÔÖÚÙÛÜÝŸỲŶ][a-zçáàäâéèêëíìîïóòôöúùûüŷỳýÿ]*['\s-]?[A-ZÇÁÀÂÄÉÈÊËÍÌÎÏÓÒÔÖÚÙÛÜÝŸỲŶ][a-zçáàäâéèêëíìîïóòôöúùûüŷỳýÿ]*/, 'i');
    return nameRegexp.test(name);
    // "Name must not contain digits nor special characters."
}

module.exports.isAttributeAvailable = async (client, attribute, attributeName, tableName) => {
    const {rows} = await client.query(`SELECT COUNT(${attributeName}) AS nb FROM ${tableName} WHERE ${attributeName} = $1`, [attribute]); // prevents 2 unique attributes from being the same
    return rows[0].nb == 0;
}

module.exports.isIdValid = async (client, id, tableName) => {
    const {rows} = await client.query(`SELECT COUNT(id) AS nb FROM ${tableName} WHERE id = $1`, [id]);
    return rows[0].nb == 1;
}

module.exports.isNotFalsy = (value) => {
    const valFalsy = [false, null, undefined, 0, "", NaN];
    return !valFalsy.includes(value);
}

module.exports.isValidDate = (date) => {
    let today = new Date(Date.now());
    today.setHours(0, 0, 0, 0);
    let isValid = false;
    if(typeof date === "string" && this.regexDate(date))
        date = this.stringToDate(date);
    if(typeof date === "object")
        isValid = today <= date;
    return isValid;
    // "Date must be today or later."
}


module.exports.stringToDate = (dateString) => {
    let parts = dateString.split('-');
    return new Date(parts[0], parts[1] - 1, parts[2]);
}

module.exports.dateToString = (date) => {
    const myDate = {
        day: date.getDate(),
        month: date.getMonth() + 1,
        year: date.getFullYear()
    };
    return `${myDate.year}-${myDate.month < 10 ? '0' : ''}${myDate.month}-${myDate.day < 10 ? '0' : ''}${myDate.day}`;
}
