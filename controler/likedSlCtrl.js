const pool = require('../model/database');
const LikedSlModel = require('../model/likedSlModel');


module.exports.createLike = async (req, res) => {
    const client = await pool.connect();
    const { survey_line, person } = req.body;

    if (isNaN(parseInt(survey_line)) || isNaN(parseInt(person)))
        res.status(400).json({ error: "surveyLine id and userId must be of 'integer' type." });
    else {
        try {
            await LikedSlModel.createLike({client, survey_line, person});
            res.status(200).json({ status: "like is created !" });
        } catch (e) {
            console.error(e);
            res.sendStatus(500);
        }finally{
            client.release();
        }
    }
}

module.exports.deleteLike = async (req, res) => {
    const client = await pool.connect();
    const { survey_line, person } = req.body;

    if (isNaN(parseInt(survey_line)) || isNaN(parseInt(person)))
        res.status(400).json({ error: "surveyLine id and userId must be of 'integer' type." });
    else {
        try {
            await LikedSlModel.deleteLike({client, survey_line, person});
            res.status(200).json({ status: "like is deleted !" });
        } catch (e) {
            console.error(e);
            res.sendStatus(500);
        }finally{
            client.release();
        }
    }
}