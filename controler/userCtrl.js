require("dotenv").config();
const process = require('process');
const jwt = require('jsonwebtoken');
const UserModel = require('../model/userModel');
const pool = require('../model/database');
const Utils = require('../utils/utils');
const TABLE_NAME = '\"user\"';

/**
 * @swagger
 * components:
 *  schemas:
 *      User:
 *          type: object
 *          properties:
 *              id:
 *                  type: integer
 *                  description: User id.
 *              firstName:
 *                  type: string
 *                  description: User's first name.
 *              lastName:
 *                  type: string
 *                  description: User's last name.
 *              email:
 *                  type: string
 *                  description: Email (format must respect 'abc@example.foo')
 *              password:
 *                  type: string
 *                  format: password
 *              address:
 *                  type: string
 *                  description: The user's address. Nullable.
 *              postalCode:
 *                  type: string
 *                  description: The postal code corresponding to the user's address. Nullable.
 *              isAdmin:
 *                  type: boolean
 *                  description: Indicates if the user is admin or not.
 *  responses:
 *      UserNotFound:
 *          description: User not found.
 */

/**
 * @swagger
 * components:
 *  schemas:
 *      Login:
 *          type: object
 *          properties:
 *              email:
 *                  type: string
 *                  description: Email (format must respect 'abc@example.foo')
 *              password:
 *                  type: string
 *                  format: password
 *          required:
 *              - email
 *              - password
 *  responses:
 *      LoginParamsError:
 *          description: Email and password are required.
 */
module.exports.login = async (req, res) => {
    const { email, password } = req.body;

   
    if (email === undefined || password === undefined)
        res.status(400).json({ error: "Email and password are required." });
    else {
        const client = await pool.connect();

        try {

            const { rows: users } = await UserModel.getUser(client, email);

            const user = users[0];
            if (user === undefined || !await Utils.compareHash(password, user.password))
                res.status(404).json({ error: "User not found." });
            else {
                const payload = {
                    status: user.is_admin ? "admin" : "user",
                    value: {
                        id: user.id,
                        first_name: user.first_name,
                        last_name: user.last_name,
                        email: user.email,
                        postal_code: user.postal_code,
                        address: user.address,
                        is_admin: user.is_admin
                    }

                };

                const token = jwt.sign(
                    payload,
                    process.env.SECRET_TOKEN,
                    { expiresIn: '1d' }
                );

                res.json(token);
            }
        } catch (e) {
            console.error(e);
            res.status(500).json({ error: "server error !" });
        } finally {
            client.release();
        }
    }
};

/**
 * @swagger
 * components:
 *   requestBodies:
 *       UserToCreate:
 *           content:
 *               application/json:
 *                   schema:
 *                       type: object
 *                       properties:
 *                          firstName:
 *                              type: string
 *                              description: User's first name.
 *                          lastName:
 *                              type: string
 *                              description: User's last name.
 *                          email:
 *                              type: string
 *                              description: Email (format must respect 'abc@example.foo')
 *                          password:
 *                              type: string
 *                              format: password
 *                          address:
 *                              type: string
 *                              description: The user's address. Optional.
 *                          postalCode:
 *                              type: string
 *                              description: The postal code corresponding to the user's address. Optional.
 *                          isAdmin:
 *                              type: boolean
 *                              description: Indicates if the user is admin or not.
 *                       required:
 *                           - firstName
 *                           - lastName
 *                           - email
 *                           - password
 *   responses:
 *       UserCreated:
 *           description: The user has been created.
 */
module.exports.createUser = async (req, res) => {
    const user = req.body;
    const client = await pool.connect();

    if (
        !Utils.regexPassword(user.password) ||
        !Utils.regexEmail(user.email) ||
        !await Utils.isAttributeAvailable(client, user.email, "email", TABLE_NAME) ||
        !Utils.regexName(user.firstName) ||
        !Utils.regexName(user.lastName)
    ) res.status(400).json({ error: "Invalid parameter(s)." });
    else {
        if (user.address === undefined) user.address = null;
        if (user.postalCode === undefined || user.postalCode === "") user.postalCode = null;
        if (typeof user.isAdmin !== "boolean") user.isAdmin = false;

        try {
            await UserModel.createUser(client, user);
            res.status(201).json({
                status: "The user has been created.", addedUser: {
                    first_name: user.firstName,
                    last_name: user.lastName,
                    email: user.email,
                    address: user.address,
                    postal_code: user.postalCode,
                    id: user.id,
                    is_admin: user.isAdmin
                }
            }); // modifié
        } catch (e) {
            console.error(e);
            res.sendStatus(500);
        } finally {
            client.release();
        }
    }
};

/**
 * @swagger
 * components:
 *  requestBodies:
 *      UserToFind:
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                         email:
 *                             type: string
 *                             description: Email (format must respect 'abc@example.foo')
 *                      required:
 *                          - email
 *  responses:
 *      UserFound:
 *          description: returns a 'User' object
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/User'
 */
module.exports.getUser = async (req, res) => {
    const email = req.body.email;

    const client = await pool.connect();

    try {
        if (await Utils.isAttributeAvailable(client, email, "email", TABLE_NAME))
            res.status(400).json("Invalid email address.");
        else {
            const { rows: users } = await UserModel.getUser(client, email);
            if (users !== undefined)
                res.json(users[0]);
            else
                res.status(404).json({ error: "User not found." });
        }
    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
};

module.exports.getUserById = async (req, res) => {
    const client = await pool.connect();
    const { id } = req.params;



    if (isNaN(parseInt(id)))
        res.status(400).json({ error: "Invalid id" });
    else {

        try {
            const { rows: user } = await UserModel.getUserById(client, id);
            if (!user)
                res.status(404).json({ error: "user not found !" });
            else {
                const userFound = {
                    first_name: user[0].first_name,
                    last_name: user[0].last_name,
                    email: user[0].email,
                    address: user[0].address,
                    postal_code: user[0].postal_code,
                    id: user[0].id,
                    is_admin: user[0].is_admin
                }
                res.status(200).json(userFound);

            }
        } catch (e) {
            console.error(e);
            res.status(500).json({ error: "something went wrong !" });
        } finally {
            client.release();
        }
    }

}

/**
 * @swagger
 * components:
 *  requestBodies:
 *      UserToUpdate:
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/User'
 *  responses:
 *      UserUpdated:
 *          description: The user has been successfully updated.
 */
module.exports.updateUser = async (req, res) => {
    const client = await pool.connect();
    const toUpdate = req.body;

    toUpdate.id = parseInt(toUpdate.id);
    toUpdate.firstName = Utils.regexName(toUpdate.firstName) ? toUpdate.firstName : undefined;
    toUpdate.lastName = Utils.regexName(toUpdate.lastName) ? toUpdate.lastName : undefined;

    try {
        if (isNaN(toUpdate.id))
            res.status(400).json({ error: "Invalid id." });

        if (!await Utils.isIdValid(client, toUpdate.id, TABLE_NAME))
            res.status(404).json({ error: "User not found." });

        await UserModel.updateUser(client, toUpdate);
        res.status(200).json({
            first_name: toUpdate.firstName,
            last_name: toUpdate.lastName,
            email: toUpdate.email,
            postal_code: toUpdate.postalCode,
            is_admin: toUpdate.isAdmin,
            address: toUpdate.address,
            id: toUpdate.id
        });

    } catch (e) {
        console.error(e);
        res.sendStatus(500);

    } finally {
        client.release();
    }

};

/**
 * @swagger
 * components: 
 *  requestBodies:
 *      UserToDelete:
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          id:
 *                              type: integer
 *                              description: Id of the referenced user
 *                          email:
 *                              type: string
 *                              description: Email of the referenced user
 *  responses:
 *      UserDeleted:
 *           description: The user has been successfully deleted.
 */
module.exports.deleteUser = async (req, res) => {
    const id = parseInt(req.body.id);
    const email = req.body.email;
    const client = await pool.connect();

    try {
        if (isNaN(id) && email === undefined)
            res.status(400).json({ error: "Invalid parameter(s)." });
        if (id !== undefined && !await Utils.isIdValid(client, id, TABLE_NAME) || email !== undefined && !await Utils.isAttributeAvailable(client, email, "email", TABLE_NAME))
            res.status(404).json({ error: "User not found." });
        else {
            await UserModel.deleteUser(client, id, email);
            res.status(204).json({ status: "The user has been successfully deleted." });
        }
    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
};


module.exports.getAllUsers = async (req, res) => {
    const client = await pool.connect();

    try {
        const { rows: users } = await UserModel.getAllUsers(client);

        if (users === undefined)
            res.status(400).json({ error: "cannot get all user" });
        else {
            
            res.status(200).json(users);
        }

    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }

}

