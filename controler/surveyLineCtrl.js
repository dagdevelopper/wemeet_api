const SurveyLineModel = require('../model/surveyLineModel');
const SurveyModel = require('../model/surveyModel');
const pool = require('../model/database');

/**
 * @swagger
 * components:
 *  schemas:
 *      SurveyLine:
 *          type: object
 *          properties:
 *              id:
 *                  type: integer
 *                  description: Id of the survey.
 *              lineNumber:
 *                  type: integer
 *                  description: Number of the specified line.
 *              label:
 *                  type: string
 *                  description: A description corresponding to this line.
 *              nbVotes:
 *                  type: integer
 *                  description: The number of votes for this line.
 *  responses:
 *      SurveyLineNotFound:
 *          description: Survey line not found.
 */

/**
 * @swagger
 * components:
 *  requestBodies:
 *      SurveyLineToCreate:
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                         survey:
 *                             type: integer
 *                             description: The ID of the survey to which the line will be added.
 *                         label:
 *                             type: string
 *                             description: A description corresponding to this line.
 *                      required:
 *                          - survey
 *                          - label
 *  responses:
 *      SurveyLineCreated:
 *          description: The new line has been added to the survey.
 */
module.exports.createSurveyLine = async (req, res) => {
    const client = await pool.connect();
    const {survey, label} = req.body;
    const nbVotes = req.body.nbVotes ?? 0;
    const liked = req.body.liked ?? false;


    try {
        const surveyExist = await SurveyModel.surveyExist(client, survey);
        const missingParam = survey === undefined || label === undefined;

        if (surveyExist && !missingParam){
            const {rows : surveyLines} = await SurveyLineModel.createSurveyLine(client, survey, label, nbVotes, liked);
            const {line_number} = surveyLines[0];

            res.status(200).json({status:"The new line has been added to the survey.", createdSurveyLine : {survey, label, nb_votes : nbVotes, liked, line_number}});
        } else
            res.status(404).json({error: "No survey assigned !"});
    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
};

/**
 * @swagger
 * components:
 *  responses:
 *      SurveyLineFound:
 *           description: returns a 'SurveyLine' object
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/SurveyLine'
 */
module.exports.getSurveyLine = async (req, res) => {
    const client = await pool.connect(); 
    const lineNumber = parseInt(req.params.lineNumber);
    const survey = parseInt(req.params.survey);

    try {
        if (isNaN(lineNumber) || isNaN(survey))
            res.sendStatus(400);
        else {
            const {rows : surveyLines} = await SurveyLineModel.getSurveyLine(client, lineNumber, survey);
            const surveyLine = surveyLines[0];

            if (surveyLine !== undefined)
                res.status(200).json(surveyLine);
            else 
                res.sendStatus(404);
        }
    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
};

// //a supprimer suremment
// module.exports.getSurveyLineBySurvey = async (req, res) => {
//     const client = await pool.connect();
//     const {survey} = req.params;

//     if (isNaN(parseInt(survey)))
//         res.status(400).json({error : "params must be an integer !"});
//     else {
//         try {
//             const {rows : surveyLines} = await SurveyLineModel.getSurveyLineBySurvey({client, survey});
//             if (surveyLines !== undefined)
//                 res.status(200).json(surveyLines);
//             else res.status(404).json({error : "surveyLines not found !"});

//         } catch (e) {
//             console.error(e);
//             res.sendStatus(500);
//         } finally {
//             client.release();
//         }
//     }
// }

/**
 * @swagger
 * components:
 *  requestBodies:
 *      SurveyLineToUpdate:
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/SurveyLine'
 *  responses:
 *      SurveyLineUpdated:
 *          description: The survey line has been updated.
 */
module.exports.updateSurveyLine = async (req, res) => {
    const client = await pool.connect();
    const {label, nbVotes, survey, lineNumber, liked} = req.body;

    try {
        const surveyLineExist = await SurveyLineModel.surveyLineExist(client, lineNumber, survey);
        const missingParam = lineNumber === undefined || survey === undefined;

        if (surveyLineExist && !missingParam) {

            await SurveyLineModel.updateSurveyLine(client, label, nbVotes, survey, lineNumber, liked);

            res.status(200).json({status:"The survey line has been updated.", updatedSurveyLine : {label, nb_votes : nbVotes, survey, line_number : lineNumber, liked}});
        } else
            res.status(404).json({error: "Survey line not found."});
    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
};

/**
 * @swagger
 * components: 
 *  requestBodies:
 *      SurveyLineToDelete:
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          lineNumber:
 *                              type: integer
 *                              description: Number of the targeted line.
 *                          survey:
 *                              type: integer
 *                              description: Id of the referenced survey
 *                      required:
 *                          - lineNumber 
 *                          - survey 
 *  responses:
 *      SurveyLineDeleted:
 *          description: The survey line has been deleted.
 */
module.exports.deleteSurveyLine = async (req, res) => {
    const client = await pool.connect();
    const {lineNumber, survey} = req.body;

    try{
        if(isNaN(parseInt(lineNumber) || isNaN(parseInt(survey))))
            res.status(400).json({error:"Survey id and lineNumber must be of 'integer' type."});
        else {
            if (await SurveyLineModel.surveyLineExist(client, lineNumber, survey)) {
                await SurveyLineModel.deleteSurveyLine(client, lineNumber, survey);
                res.status(200).json({status : 'survey line deleted !'});
            } else
                res.status(404).json({error:"Survey line not found."});
        }
    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
};
