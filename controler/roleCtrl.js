const pool = require('../model/database');
const RoleModel = require('../model/roleModel');
const Utils = require("../utils/utils");
const TABLE_NAME = "role";

/**
 * @swagger
 * components:
 *  schemas:
 *      Role:
 *          type: object
 *          properties:
 *              label:
 *                  type: string
 *                  description: Name of the role.
 *  responses:
 *      RoleNotFound:
 *          description: Role not found.
 */

/**
 * @swagger
 * components:
 *  requestBodies:
 *      RoleToCreate:
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                         label:
 *                             type: string
 *                             description: Name of the role.
 *                      required:
 *                          - label
 *  responses:
 *      RoleCreated:
 *          description: The role has been created.
 */
module.exports.createRole = async (req, res) => {
    const label = req.body.label;
    const client = await pool.connect();

    if(!await Utils.isAttributeAvailable(client, label, "label", TABLE_NAME) || label === undefined)
        res.sendStatus(400);
    else {
        try {
            await RoleModel.createRole(client, label);
            res.sendStatus(201);
        } catch (e) {
            console.log(e);
            res.sendStatus(500);
        } finally {
            client.release();
        }
    }
};

/**
 * @swagger
 * components:
 *  responses:
 *      RoleFound:
 *           description: returns a 'Role' object
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/Role'
 */
module.exports.getRole = async (req, res) => {
    const label = req.params.label;
    if(label === undefined)
        res.sendStatus(400);
    else {
        const client = await pool.connect();
        try {
            const {rows: roles} = await RoleModel.getRole(client, label);
            const role = roles[0];
            if(role !== undefined)
                res.json(role);
            else
                res.sendStatus(404);
        } catch (e) {
            console.log(e);
            res.sendStatus(500);
        } finally {
            client.release();
        }
    }
};

/**
 * @swagger
 * components:
 *  requestBodies:
 *      RoleToUpdate:
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                         oldLabel:
 *                             type: string
 *                             description: Old name of the role.
 *                         newLabel:
 *                             type: string
 *                             description: New name of the role.
 *                      required:
 *                          - oldLabel
 *                          - newLabel
 *  responses:
 *       RoleUpdated:
 *           description: The Event has been updated.
 */
module.exports.updateRole = async (req, res) => {
    const oldLabel = req.body.oldLabel;
    const newLabel = req.body.newLabel;

    const client = await pool.connect();
    try {
        if(newLabel === undefined || oldLabel === undefined)
            res.sendStatus(400);
        else {
            await RoleModel.updateRole(client, oldLabel, newLabel);
            res.status(200).json({message : "Role successfully updated."});
        }
    } catch (error) {
        console.error(error);
        res.sendStatus(500);
    } finally {
        client.release();
    }
};

/**
 * @swagger
 * components: 
 *  requestBodies:
 *      RoleToDelete:
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          label:
 *                              type: string
 *                              description: Label of the referenced role
 *                      required:
 *                          - label 
 *  responses:
 *       RoleDeleted:
 *           description: The Event has been deleted.
 */
module.exports.deleteRole = async (req, res) => {
    const label = req.body.label;

    const client = await pool.connect();
    try {
        if(label === undefined || await Utils.isAttributeAvailable(client, label, "label", TABLE_NAME))
            req.sendStatus(400);
        else {
            await RoleModel.deleteRole(client, label);
            res.sendStatus(204);
        }
    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
};
