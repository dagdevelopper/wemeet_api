const pool = require('../model/database');
const { isIdValid, isAttributeAvailable } = require("../utils/utils");
const EventRoleModel = require('../model/eventRoleModel');
const EventModel = require("../model/eventModel");
const UserModel = require("../model/userModel");

/**
 * @swagger
 * components:
 *  schemas:
 *      EventRole:
 *          type: object
 *          properties:
 *              "user":
 *                  type: integer
 *                  description: Id of the referenced user for the specific role concerning the specific event, part of the primary key
 *              event:
 *                  type: integer
 *                  description: Id of the referenced event for the specific role concerning the specific user, part of the primary key
 *              role:
 *                  type: string
 *                  description: Id of the referenced role for the specific user concerning the specific event, part of the primary key
 *  responses:
 *      IllegalArgumentEventRole:
 *          description: Bad id (user, event or role). User and event must be of 'interger' type. Role must be of 'string' type.
 */

/**
 * @swagger
 * components:
 *  requestBodies:
 *      EventRoleToCreate:
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/EventRole'
 *  responses:
 *      EventRoleCreated:
 *          description: The role has been added to the user for this event.
 */
module.exports.createEventRole = async (req, res) => {
    let { userId, eventId, role } = req.body;
    const client = await pool.connect();

    if (!role)
        role = 'participant';

    try {
        if (
            !await isIdValid(client, userId, "\"user\"")
            || !await isIdValid(client, eventId, "event")
            || await isAttributeAvailable(client, role, "label", "role")
        ) res.sendStatus(400);

        await EventRoleModel.createEventRole(client, userId, eventId, role);
        const { rows: users } = await UserModel.getUserById(client, userId);
        users[0].role = role;

        res.status(201).json({ status: "Event role successfully created.", user: users[0], eventId });
    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
};


/**
 * @swagger
 * components:
 *  requestBodies:
 *      EventRoleToVerify:
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/EventRole'
 *  responses:
 *      EventRoleFound:
 *           description: returns a 'EventRole' object
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/EventRole'
 */
module.exports.eventRoleExists = async (req, res) => {
    const user = req.body.user;
    //const role = req.body.role;
    const event = req.body.event;
    const client = await pool.connect();

    try {
        if (
            !await isIdValid(client, user, "\"user\"")
            || !await isIdValid(client, event, "event")
            //|| await isAttributeAvailable(client, role, "label", "role")
        ) res.sendStatus(400);
        res.status(200).json(await EventRoleModel.getEventRole(client, user, event));
    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
};

/**
 * @swagger
 * components:
 *  responses:
 *      AllEventRolesFound:
 *           description: returns a list of all 'EventRole' objects found for the specific event.
 *           content:
 *               application/json:
 *                   schema:
 *                       type: array
 *                       items:
 *                          $ref: '#/components/schemas/EventRole'
 */
module.exports.getEventRoles = async (req, res) => {
    const event = req.params.eventId;
    const client = await pool.connect();

    try {

        if (!await isIdValid(client, event, "event")) {
            res.status(400).json({ error: "Invalid event id." });
        } else {
            const { rows: eventRoles } = await EventRoleModel.getEventRoles(client, event);

            if (eventRoles !== undefined) {
                res.status(200).json(eventRoles);
            } else {
                res.status(404).json({ error: "event role not found ! " });
            }
        }
    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
};


//new Function
module.exports.getEventRoleByUser = async (req, res) => {
    const client = await pool.connect();
    const userId = req.params.userId;

    try {
        if (!await isIdValid(client, userId, "\"user\""))
            res.status(400).json({ error: 'Invalid user id' });
        else {
            const { rows: eventRoles } = await EventRoleModel.getEventRoleByUser(client, userId);

            if (eventRoles !== undefined) {
                const participated = [];
                const organized = [];

                for (const er of eventRoles) {
                    const { rows: events } = await EventModel.getEvent(client, er.event);
                    const { rows: participants } = await UserModel.getUsersByEvent(client, events[0]?.id);

                    events[0].participants = participants;
                    events[0].creation_date = new Date(parseInt(events[0].creation_date));

                    if (er.role === 'participant') {
                        participated.unshift(events[0]);
                    } else if (er.role === 'organizer') {
                        organized.unshift(events[0]);
                    }
                }

                const datas = { participated: participated, organized: organized }

                res.status(200).json(datas);
            } else
                res.status(404).json({ error: 'no event role with this user ID !' });
        }
    } catch (error) {
        console.error(error);
        res.sendStatus(500);
    } finally {
        client.release();
    }


}

/**
 * @swagger
 * components:
 *  requestBodies:
 *      EventRoleToUpdate:
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/EventRole'
 *  responses:
 *      EventRoleUpdated:
 *          description: The EventRole has been updated.
 */
module.exports.updateEventRole = async (req, res) => {
    const user = req.body.user;
    const role = req.body.role;
    const event = req.body.event;
    const client = await pool.connect();

    try {
        if (
            !await isIdValid(client, user, "\"user\"")
            || !await isIdValid(client, event, "event")
            || await isAttributeAvailable(client, role, "label", "role")
        ) res.sendStatus(400);
        await EventRoleModel.updateEventRole(client, user, event, role);
        res.status(204).json({ message: "Event role successfully updated." });
    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
};


/**
 * @swagger
 * components: 
 *  requestBodies:
 *      EventRoleToDelete:
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          user:
 *                              type: integer
 *                              description: Id of the referenced user
 *                          role:
 *                              type: string
 *                              description: Label of the referenced role
 *                          event:
 *                              type: integer
 *                              description: Id of the referenced event
 *                      required:
 *                          - user
 *                          - role
 *                          - event
 *  responses:
 *      EventRoleDeleted:
 *          description: The EventRole has been deleted.
 */

module.exports.deleteEventRole = async (req, res) => {
    const user = req.body.user;
    //const role = req.body.role;
    const event = req.body.event;
    const client = await pool.connect();



    try {
        if (!await EventRoleModel.eventRoleExists({ client, user, event }))
            res.sendStatus(400);
            
        await EventRoleModel.deleteEventRole(client, user, event);

        res.status(201).json({ status: "Event role successfully deleted.", userId: user, eventId: event });
    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
};


