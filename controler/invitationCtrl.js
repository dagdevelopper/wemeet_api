const pool = require("../model/database");
const InvitationModel = require("../model/invitationModel");
const UserModel = require("../model/userModel");
const EventModel = require("../model/eventModel");
const Utils = require("../utils/utils");
const TABLE_NAME = '"user"';

module.exports.getInvitationsByEventId = async (req, res) => {
  const client = await pool.connect();
  const { eventId } = req.params;

  try {
    if (!(await EventModel.eventExist(client, eventId))) {
      res.status(404).json({ error: "Event not found." });
      return;
    }

    const { rows: invitations } = await InvitationModel.getInvitationsByEventId(
      {
        client,
        eventId,
      }
    );

    for (const invitation of invitations) {
      const { rows: users } = await UserModel.getUserById(
        client,
        invitation.sender
      );
      users && (invitation.sender = users[0].email);
    }

    res.status(200).json({ invitations });
  } catch (e) {
    console.error(e);
    res.sendStatus(500);
  } finally {
    client.release();
  }
};

module.exports.getInvitations = async (req, res) => {
  const client = await pool.connect();
  const { email } = req.params;

  try {
    if (await Utils.isAttributeAvailable(client, email, "email", TABLE_NAME)) {
      res.status(404).json({ error: "User not found." });
      return;
    }

    const { rows: invitations } = await InvitationModel.getInvitations({
      client,
      email,
    });
    const invites = [];
    let nbUnread = 0;

    if (invitations) {
      invitations.sort((a, b) => a.sender - b.sender);

      let i = 0;
      while (i < invitations.length) {
        const invite = {
          sender: {},
          invitations: [],
        };

        const sameSender = invitations[i].sender;
        const { rows: users } = await UserModel.getUserById(client, sameSender);
        invite.sender = users[0];

        while (i < invitations.length && invitations[i].sender === sameSender) {
          const { rows } = await EventModel.getEvent(
            client,
            invitations[i].event
          );

          if (new Date(Date.now()) > new Date(rows[0].date_end)) {
            //supprime invite
            await InvitationModel.deleteInvitation({
              client,
              guest: email,
              eventId: rows[0].id,
            });
          } else {
            let { rows: participants } = await UserModel.getUsersByEvent(
              client,
              rows[0].id
            );

            rows[0].participants = participants;

            const info = {
              accepted: invitations[i].accepted,
              event: rows[0],
            };

            !info.accepted && nbUnread++;
            invite.invitations.push(info);
          }

          i++;
        }
        invite.invitations.sort((a, b) => a.accepted - b.accepted);
        invites.push(invite);
      }

      res.status(200).json({ invites, nbUnread });
    } else {
      res.sendStatus(401);
    }
  } catch (e) {
    console.error(e);
    res.sendStatus(500);
  } finally {
    client.release();
  }
};

module.exports.createInvitation = async (req, res) => {
  const client = await pool.connect();
  let { userId, eventId, guest, accepted } = req.body;
  accepted = false;

  if (isNaN(parseInt(userId)) || isNaN(parseInt(eventId)))
    res
      .status(400)
      .json({ error: "Illegal parameter. ID must be of type 'integer'." });

  try {
    if (await Utils.isAttributeAvailable(client, guest, "email", TABLE_NAME)) {
      res.status(404).json({ error: "C'est utilisateur n'a pas de compte !" });
      return;
    }

    if (await InvitationModel.invitationExists({ client, guest, eventId }))
      res.status(405).json({ error: "invitation already exists !" });
    else {
      await InvitationModel.createInvitation({
        client,
        userId,
        eventId,
        guest,
        accepted,
      });
      res.status(200).json({ status: "invitation created !" });
    }
  } catch (error) {
    console.error(error);
    res.sendStatus(500);
  } finally {
    client.release();
  }
};

module.exports.deleteInvitationByEvent = async (req, res) => {
  const client = await pool.connect();
  const { eventId } = req.params;

  if (isNaN(parseInt(eventId)))
    res.status(400).json({
      error: "Illegal parameter. Event ID must be of type 'integer'.",
    });

  try {
    if (!(await InvitationModel.invitationsExists({ client, eventId }))) {
      res
        .status(404)
        .json({ error: "invitations not exists for this event !" });
     
    } else {
      await InvitationModel.deleteInvitationByEvent({ client, eventId });
      res.status(200).json({ status: "invitations deleted !" });
     
    }
  } catch (error) {
    console.error(error);
    res.sendStatus(500);
  } finally {
    client.release();
  }
};

module.exports.deleteInvitation = async (req, res) => {
  const client = await pool.connect();
  const { guest, eventId } = req.body;

  if (isNaN(parseInt(eventId)))
    res
      .status(400)
      .json({ error: "Illegal parameter. ID must be of type 'integer'." });

  try {
    if (!(await InvitationModel.invitationExists({ client, guest, eventId }))) {
      res.status(405).json({ error: "invitation not exists !" });
      return;
    }

    await InvitationModel.deleteInvitation({ client, guest, eventId });
    res.status(200).json({ status: "invitation deleted !" });
  } catch (error) {
    console.error(error);
    res.sendStatus(500);
  } finally {
    client.release();
  }
};

module.exports.updateInvitation = async (req, res) => {
  const client = await pool.connect();
  const { guest, eventId, accepted } = req.body;

  if (isNaN(parseInt(eventId)))
    res
      .status(400)
      .json({ error: "Illegal parameter. ID must be of type 'integer'." });

  try {
    if (!(await InvitationModel.invitationExists({ client, guest, eventId }))) {
      res.status(405).json({ error: "invitation not exists !" });
      return;
    }

    await InvitationModel.updateInvitation({
      client,
      guest,
      eventId,
      accepted,
    });
    res.status(200).json({ status: "invitation updated !" });
  } catch (error) {
    console.error(error);
    res.sendStatus(500);
  } finally {
    client.release();
  }
};
