const SurveyModel = require('../model/surveyModel');
const EventModel = require('../model/eventModel');
const pool = require('../model/database');
const Utils = require("../utils/utils");
const TABLE_NAME = "survey";
const SurveyLineModel = require('../model/surveyLineModel');
const LikedSlModel = require('../model/likedSlModel');

/**
 * @swagger
 * components:
 *  schemas:
 *      Survey:
 *          type: object
 *          properties:
 *              id:
 *                  type: integer
 *                  description: Id of the survey.
 *              label:
 *                  type: string
 *                  description: Title of the survey.
 *              description:
 *                  type: string
 *                  description: Description of the survey.
 *              event:
 *                  type: integer
 *                  description: Id of the event that contains this survey.
 *  responses:
 *      SurveyNotFound:
 *          description: Survey not found.
 */

/**
 * @swagger
 * components:
 *  requestBodies:
 *      SurveyToCreate:
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                         label:
 *                             type: string
 *                             description: Title of the survey.
 *                         description:
 *                             type: string
 *                             description: Description of the survey.
 *                         event:
 *                             type: integer
 *                             description: Id of the event that contains this survey.
 *                      required:
 *                          - label
 *                          - event
 *  responses:
 *      SurveyCreated:
 *          description: The survey has been created.
 */
module.exports.createSurvey = async (req, res) => {

    const client = await pool.connect();
    const { label, description, event } = req.body;

    try {
        const eventExist = await EventModel.eventExist(client, event);
        if (eventExist) {
            const { rows: surveys } = await SurveyModel.createSurvey(client, event, label, description);

            res.status(201).json({ status: "The survey has been created.", createdSurvey: { title: label, description, event, id: surveys[0].id, surveyLines: [{ survey: surveys[0].id }] } });
        } else {
            res.status(405).json({ error: "No event assigned." });
        }
    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
};

/**
 * @swagger
 * components:
 *  requestBodies:
 *      SurveyToUpdate:
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                         id:
 *                             type: integer
 *                             description: Id of the survey.
 *                         label:
 *                             type: string
 *                             description: Title of the survey.
 *                         description:
 *                             type: string
 *                             description: Description of the survey.
 *                      required:
 *                          - id
 *  responses:
 *      SurveyUpdated:
 *          description: The survey has been updated.
 */
module.exports.updateSurvey = async (req, res) => {
    const client = await pool.connect();
    const { id, label, description } = req.body;

    try {
        if (isNaN(parseInt(id)))
            res.sendStatus(400);
        else {
            if (!await Utils.isIdValid(client, id, TABLE_NAME))
                res.status(404).json({ error: "Survey not found." });
        }
        await SurveyModel.updateSurvey(client, id, label, description);
        res.status(201).json({ status: "The survey has been updated." });
    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
};

/**
 * @swagger
 * components:
 *  responses:
 *      SurveyFound:
 *           description: returns a 'Survey' object
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/Survey'
 */
module.exports.getSurveysByEventId = async (req, res) => {
    const client = await pool.connect();
    const id = parseInt(req.params.eventId);
    const userId = req.params.userId;

    try {
        if (isNaN(id))
            res.sendStatus(400);
        else {
            let { rows: surveys } = await SurveyModel.getSurveysByEventId(client, id);
            if (surveys !== undefined) {
                for (let s of surveys) {
                    const survey = s.id;
                    const { rows: surveyLines } = await SurveyLineModel.getSurveyLineBySurvey({ client, survey });

                    if (surveyLines === undefined || surveyLines.length === 0)
                        s.surveyLines = [{ survey }]
                    else {
                        for (let sl of surveyLines) {
                            const { rows: likes } = await LikedSlModel.getLikes({ client, survey_line: sl.line_number, person: userId });
                            const { rows: allLikes } = await LikedSlModel.getAllLikes({ client, survey_line: sl.line_number });

                            if (likes.length !== 0) {
                                const like = likes[0];
                                const res = allLikes.find(l => l.survey_line === like.survey_line && l.person === like.person);

                                sl.liked = res !== undefined;
                            } else
                                sl.liked = false;
                            sl.nb_votes = allLikes.length;
                        }

                        s.surveyLines = surveyLines;
                    }
                }

                res.json(surveys);
            } else
                res.status(404).json({ error: "Survey not found." });
        }
    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
};

/**
 * @swagger
 * components: 
 *  requestBodies:
 *      SurveyToDelete:
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          id:
 *                              type: integer
 *                              description: Id of the referenced survey
 *                      required:
 *                          - id 
 *  responses:
 *      SurveyDeleted:
 *          description: The survey has been deleted.
 */
module.exports.deleteSurvey = async (req, res) => {
    const client = await pool.connect();
    const { id } = req.body;

    try {
        if (isNaN(parseInt(id)))
            res.sendStatus(400);
        else {
            if (!await Utils.isIdValid(client, id, TABLE_NAME))
                res.status(404).json({ error: "Survey not found." });
            await SurveyModel.deleteSurvey(client, id);
            res.status(201).json({ status: "The survey has been deleted." });
        }
    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
};
