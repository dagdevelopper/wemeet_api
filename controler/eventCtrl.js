const EventModel = require('../model/eventModel');
const UserModel = require('../model/userModel');
const EventRoleModel = require('../model/eventRoleModel');
const InvitationModel = require('../model/invitationModel');
const pool = require('../model/database');
const { isValidDate, stringToDate } = require('../utils/utils');

/**
 * @swagger
 * components:
 *  schemas:
 *      Event:
 *          type: object
 *          properties:
 *              id:
 *                  type: integer
 *                  description: Id of the event.
 *              label:
 *                  type: string
 *                  description: Title of the event.
 *              description:
 *                  type: string
 *                  description: Description of the event.
 *              cityName:
 *                  type: string
 *                  description: Name of the city where the event will take place.
 *              isPrivate:
 *                  type: boolean
 *                  description: Indicates if the event is private or public.
 *  responses:
 *      EventNotFound:
 *          description: Event not found.
 */

/**
 * @swagger
 * components:
 *  responses:
 *      EventFound:
 *           description: returns a 'Event' object
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/Event'
 */
module.exports.getEvent = async (req, res) => {
    const client = await pool.connect();
    const id = parseInt(req.params.id);

    try {
        if (isNaN(id)) {
            res.sendStatus(400);
        } else {
            const { rows: events } = await EventModel.getEvent(client, id);
            const event = events[0];
            if (event !== undefined) {
                let { rows: participants } = await UserModel.getUsersByEvent(client, event.id);
                event.participants = participants;
                res.json(event);
            } else
                res.sendStatus(404);
        }
    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
}

/**
 * @swagger
 * components:
 *  responses:
 *      EventsList:
 *           description: returns a list of all events found in DB.
 *           content:
 *               application/json:
 *                   schema:
 *                       type: array
 *                       items:
 *                          $ref: '#/components/schemas/Event
 *      NoEventFound:
 *           description: No event has been found. 
 */
module.exports.getAllEvents = async (req, res) => {
    const client = await pool.connect();
    const { email } = req.params;

    try {
        let { rows: events } = await EventModel.getAllEvent(client);

        if (events !== undefined) {
            for (const e of events) {
                let { rows: participants } = await UserModel.getUsersByEvent(client, e.id);

                //sil ya une invit acceptée de ma part sur cet event, et que je ne suis pas encore parmi les participants, je m'ajoute
                e.nbParticipants = participants.length;
                e.participants = participants;

                if (email) {
                    if (await InvitationModel.invitationAcceptedExists({ client, guest: email, eventId: e.id })) {
                        const { rows: users } = await UserModel.getUser(client, email);

                        if (users) {
                            const user = e.participants.find(p => p.id === users[0].id);
                            !user && e.participants.push(users[0]);
                        }
                    }
                }

                e.creation_date = new Date(parseInt(e.creation_date));
            }

            res.status(200).json(events.reverse());
        } else {
            res.status(404).json({ error: "Event not found." });
        }

    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
}


/**
 * @swagger
 * components:
 *  responses:
 *      ParticipantsList:
 *           description: returns a list of all 'User' objects found for the specific event.
 *           content:
 *               application/json:
 *                   schema:
 *                       type: array
 *                       items:
 *                          $ref: '#/components/schemas/User'
 */
module.exports.getParticipants = async (req, res) => {
    const client = await pool.connect();
    const eventId = parseInt(req.params.id);

    try {
        if (isNaN(eventId)) {
            res.sendStatus(400);
        } else {
            const { rows: participants } = await UserModel.getUsersByEvent(client, eventId);
            if (participants !== undefined)
                res.status(200).json(participants);
            else
                res.sendStatus(404);
        }
    } catch (e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
}

/**
 * @swagger
 * components:
 *  requestBodies:
 *      EventToCreate:
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                         label:
 *                             type: string
 *                             description: Title of the event.
 *                         description:
 *                             type: string
 *                             description: Description of the event.
 *                         cityName:
 *                             type: string
 *                             description: Name of the city where the event will take place.
 *                         isPrivate:
 *                             type: boolean
 *                             description: Indicates if the event is private or public.
 *                         email:
 *                             type: string
 *                             description: Email of the 'organizer' user.
 *                      required:
 *                          - label
 *                          - cityName
 *                          - email
 *  responses:
 *      EventCreated:
 *          description: The event has been created.
 */
module.exports.createEvent = async (req, res) => {
    const client = await pool.connect();
    const email = req.body.email;
    const label = req.body.label;
    const cityName = req.body.cityName;
    const description = req.body.description ?? null;
    const isPrivate = typeof req.body.isPrivate === "boolean" ? req.body.isPrivate : false;
    const dateBegin = req.body.dateBegin;
    const dateEnd = req.body.dateEnd;
    const creation_date = new Date(Date.now()).getTime();


    if (email === undefined || label === undefined || cityName === undefined || dateBegin === undefined || dateEnd === undefined)
        res.status(400).json({ error: "Bad parameter(s)." });
    else if (!isValidDate(dateBegin) || !isValidDate(dateEnd)) {
        res.status(401).json({ error: "Invalid dateTime !" });
    } else {
        try {
            const { rows: users } = await UserModel.getUser(client, email);
            if (users === undefined) {
                res.status(404).json({ error: "User not found." });
            } else {

                await client.query("BEGIN;");
                const role = 'organizer';
                const date_begin = stringToDate(dateBegin);
                const date_end = stringToDate(dateEnd);
                const user = users[0];
                const { rows: events } = await EventModel.createEvent(client, label, description, cityName, isPrivate, date_begin, date_end, creation_date);
                const { id } = events[0];
                user.role = 'organizer';
                await EventRoleModel.createEventRole(client, user.id, id, role);
                await client.query("COMMIT;");

                res.status(201).json({
                    status: "The Event has been created.", createdEvent: {
                        id, label, description,
                        city_name: cityName, is_private: isPrivate,
                        date_begin: dateBegin,
                        date_end: dateEnd,
                        creation_date: new Date(creation_date),
                        nbParticipants: 1,
                        participants: [user]
                    }
                });
            }
        } catch (e) {
            await client.query("ROLLBACK;");
            console.error(e);
            res.sendStatus(500);
        } finally {
            client.release();
        }
    }
}

/**
 * @swagger
 * components:
 *  requestBodies:
 *      EventToUpdate:
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                         label:
 *                             type: string
 *                             description: Title of the event.
 *                         description:
 *                             type: string
 *                             description: Description of the event.
 *                         cityName:
 *                             type: string
 *                             description: Name of the city where the event will take place.
 *                         isPrivate:
 *                             type: boolean
 *                             description: Indicates if the event is private or public.
 *                      required:
 *                          - id
 *  responses:
 *      EventUpdated:
 *          description: The Event has been updated.
 */
module.exports.updateEvent = async (req, res) => {
    const id = parseInt(req.body.id);
    const label = req.body.label;
    const cityName = req.body.cityName;
    const description = req.body.description;
    const isPrivate = typeof req.body.isPrivate === "boolean" ? req.body.isPrivate : false;
    const client = await pool.connect();

    if (!isNaN(id)) {

        const eventExist = await EventModel.eventExist(client, id);
        if (!eventExist)
            res.status(404).json({ error: "Event not found." });
        else {
            try {
                await EventModel.updateEvent(client, id, label, description, cityName, isPrivate);
                res.status(201).json({ status: "The Event has been updated." });
            } catch (e) {
                console.error(e);
                res.sendStatus(500);
            } finally {
                client.release();
            }
        }
    } else
        res.status(400).json({ error: "Illegal parameter. ID must be of type 'integer'." });
}

/**
 * @swagger
 * components: 
 *  requestBodies:
 *      EventToDelete:
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          id:
 *                              type: integer
 *                              description: Id of the referenced event
 *                      required:
 *                          - id 
 *  responses:
 *      EventDeleted:
 *          description: The Event has been deleted.
 */
module.exports.deleteEvent = async (req, res) => {

    const client = await pool.connect();
    const { id } = req.body;

    if (isNaN(parseInt(id))) {

        res.status(400).json({ error: "Illegal parameter. ID must be of type 'integer'." });
    } else {
        const eventExist = await EventModel.eventExist(client, id);
        if (!eventExist)
            res.status(404).json({ error: "Event not found." });
        else {
            try {
                await EventModel.deleteEvent(client, id);
                res.status(200).json({ status: "Event deleted.", eventDeleted: id });
            } catch (e) {
                console.error(e);
                res.sendStatus(500);
            } finally {
                client.release();
            }
        }
    }
};
