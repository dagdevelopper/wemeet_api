const CalendarModel = require('../model/calendarModel');
const pool = require('../model/database');
const Utils = require('../utils/utils');
const EventModel = require('../model/eventModel');

/**
 * @swagger
 * components:
 *  schemas:
 *      Calendar:
 *          type: object
 *          properties:
 *              event:
 *                  type: integer
 *                  description: Id of the referenced event, part of the primary key.
 *              date:
 *                  type: object
 *                  format: Date
 *                  description: Suggested date for the event to take place.
 *              nbYes:
 *                  type: integer
 *                  description: Number of 'yes' votes for the suggested date.
 *              nbNo:
 *                  type: integer
 *                  description: Number of 'no' votes for the suggested date.
 *              nbMaybe:
 *                  type: integer
 *                  description: Number of 'maybe' votes for the suggested date.
 *  responses:
 *     CalendarNotFound:
 *         description: No calendar found.
 */

/**
 * @swagger
 * components:
 *  responses:
 *      AllCalendarsFound:
 *           description: returns a list of all 'Calendar' objects found
 *           content:
 *               application/json:
 *                   schema:
 *                       type: array
 *                       items:
 *                          $ref: '#/components/schemas/Calendar'
 */
module.exports.getAllCalendars = async (req, res) => {
    const eventId = parseInt(req.params.id);
    const client = await pool.connect();

    try {
        if(isNaN(eventId))
            res.status(400).json({error: "Illegal parameter. ID must be of type 'integer'."});
        else {
            let {rows: calendars} = await CalendarModel.getAllCalendars(client, eventId);
            if (calendars !== undefined) {
                calendars.forEach(c => c.date = Utils.dateToString(c.date));
                res.status(200).json(calendars);
            }
            else
                res.status(404).json({error: "No calendar found."});
        }
    } catch(e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
};

/**
 * @swagger
 * components:
 *  responses:
 *      CalendarFound:
 *           description: returns a 'Calendar' object
 *           content:
 *               application/json:
 *                   schema:
 *                       $ref: '#/components/schemas/Calendar'
 */
module.exports.getCalendar = async (req, res) => {
    const client = await pool.connect();
    const eventId = parseInt(req.params.id);
    let date = req.params.date;

    if (Utils.isValidDate(date)){
        try {
            if (isNaN(eventId))
                res.sendStatus(400);
            else {
                const {rows : calendars} = await CalendarModel.getCalendar(client, eventId, date);
                const calendar = calendars[0];
                calendar.date = Utils.dateToString(calendar.date);
                res.json(calendar);
            }
        } catch(e) {
            console.error(e);
            res.sendStatus(500);
        } finally {
            client.release();
        }
    } else
        res.status(404).json({error : "date format must be : YYYY-MM-DD"});
};

/**
 * @swagger
 * components:
 *  responses:
 *      CalendarCreated:
 *          description: The Calendar has been created.
 *  requestBodies:
 *      CalendarToCreate:
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          eventId:
 *                              type: integer
 *                              description: Id of the referenced event
 *                          date:
 *                              type: object
 *                              format: Date
 *                              description: Suggested date for the event to take place.
 *                      required:
 *                          - eventId
 *                          - date
 */
module.exports.createCalendar = async (req, res) => {
    const client = await pool.connect();
    let {eventId, date} = req.body;

    try {
        const eventExist = await EventModel.eventExist(client, eventId);
        const calendarExist = await CalendarModel.calendarExist(client, eventId, date);

        if (eventExist && date !== null) {
            if (!calendarExist) {

                date = Utils.stringToDate(date);
                await CalendarModel.createCalendar(client, eventId, date);
                res.status(201).json({status : "calendar has been created !"});
            } else {
                res.status(400).json({error : "calendar already exist"});
            }
        } else
            res.status(404).json({error : "No event assigned"});
    } catch(e) {
        console.error(e);
        res.sendStatus(500);
    } finally {
        client.release();
    }
};

/**
 * @swagger
 * components:
 *  responses:
 *      CalendarUpdated:
 *          description: The Calendar has been updated.
 *  requestBodies:
 *      CalendarToUpdate:
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          eventId:
 *                              type: integer
 *                              description: Id of the referenced event.
 *                          oldDate:
 *                              type: object
 *                              format: Date
 *                              description: Old value for the targeted date.
 *                          newDate:
 *                              type: object
 *                              format: Date
 *                              description: New value for the targeted date.
 *                      required:
 *                          - eventId
 *                          - oldDate
 *                          - newDate
 */
module.exports.updateCalendar = async (req, res) => {
    const client = await pool.connect();
    const {eventId, oldDate, newDate} = req.body; 

    try {
        const calendarExist = await CalendarModel.calendarExist(client, eventId, oldDate);

        if (calendarExist === true && (newDate !== undefined && newDate !== null)) {
            await CalendarModel.updateCalendar(client, eventId, oldDate, newDate);
            res.status(201).json({status : "Calendar has been updated successfully !"});
        } else {
            res.status(404).json({error: "No calendar assigned or param request"});
        }
    } catch(e) {
        console.error(e);
        res.status(500).json({error: "Problem with the Database !"});
    } finally{
        client.release();
    }
};

/**
 * @swagger
 * components: 
 *  requestBodies:
 *      CalendarToDelete:
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                          eventId:
 *                              type: integer
 *                              description: Id of the referenced event
 *                          date:
 *                              type: object
 *                              format: Date
 *                              description: Suggested date for the event to take place.
 *                      required:
 *                          - eventId
 *                          - date
 *  responses:
 *      CalendarDeleted:
 *          description: The calendar has been deleted.
 */
module.exports.deleteCalendar = async (req, res) => {
    const client = await pool.connect();
    const {eventId, date} = req.body;

    try {
        const calendarExist = await CalendarModel.calendarExist(client, eventId, date);

        if (calendarExist){
            await CalendarModel.deleteCalendar(client, eventId, date);
            res.status(201).json({status: "The calendar has been deleted."});
        }else {
            res.status(404).json({error: "No calendar assigned"});
        }
    }catch(e) {
        console.error(e);
        res.sendStatus(500);
    }finally{
        client.release();
    }
};
