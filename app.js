const Router = require('./router');
const express = require('express');
//const internalIp = require('internal-ip')()
const internalIp = require('internal-ip')
const internalIP = internalIp.v4.sync();
const cors = require('cors');
require("dotenv").config();
const app = express();
const port = process.env.API_PORT;


app.use(cors());
app.use(express.json());
app.use(Router);

app.listen(port, internalIP, () => {
    console.log(`Wemeet listening at http://${internalIP}:${port}`);
});
