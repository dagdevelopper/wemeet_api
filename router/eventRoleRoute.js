const EventRoleCtrl = require("../controler/eventRoleCtrl");
const JWTMiddleware = require('../middleware/IdentificationJWT');
const Router = require('express-promise-router');
const router = new Router;
/**
 * @swagger
 * /eventRole:
 *  get:
 *      tags:
 *          - EventRole
 *      responses:
 *          200:
 *              $ref: '#/components/responses/EventRoleFound'
 *          400:
 *              $ref: '#/components/responses/IllegalArgumentEventRole'
 *          404:
 *              description: No role found for this user concerning this event.
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.get('/', EventRoleCtrl.eventRoleExists);
/**
 * @swagger
 * /eventRole/{eventId}:
 *  get:
 *      tags:
 *          - EventRole
 *      parameters:
 *          - name: eventId
 *            description: ID of the event
 *            in: path
 *            required: true
 *            schema:
 *              type: integer
 *      responses:
 *          200:
 *              $ref: '#/components/responses/AllEventRolesFound'
 *          400:
 *              $ref: '#/components/responses/IllegalArgumentEventRole'
 *          404:
 *              description: Event role not found.
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.get('/:eventId', EventRoleCtrl.getEventRoles);
/**
 * @swagger
 * /eventRole:
 *  post:
 *      tags:
 *          - EventRole
 *      security:
 *          - bearerAuth: []
 *      requestBody:
 *          $ref: '#/components/requestBodies/EventRoleToCreate'
 *      responses:
 *          201:
 *              $ref: '#/components/responses/EventRoleCreated'
 *          400:
 *              $ref: '#/components/responses/ErrorJWT'
 *          401:
 *              $ref: '#/components/responses/MissingJWT'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */

//------------------------------------------------------
router.get('/roles/:userId', EventRoleCtrl.getEventRoleByUser);
//------------------------------------------------------




router.post('/', JWTMiddleware.identification, EventRoleCtrl.createEventRole);
/**
 * @swagger
 * /eventRole:
 *  patch:
 *      tags:
 *          - EventRole
 *      security:
 *          - bearerAuth: []
 *      requestBody:
 *          $ref: '#/components/requestBodies/EventRoleToUpdate'
 *      responses:
 *          204:
 *              $ref: '#/components/responses/EventRoleUpdated'
 *          400:
 *              $ref: '#/components/responses/ErrorJWT'
 *          401:
 *              $ref: '#/components/responses/MissingJWT'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.patch('/', JWTMiddleware.identification, EventRoleCtrl.updateEventRole);
/**
 * @swagger
 * /eventRole:
 *  delete:
 *      tags:
 *          - EventRole
 *      security:
 *          - bearerAuth: []
 *      requestBody:
 *          $ref: '#/components/requestBodies/EventRoleToDelete'
 *      responses:
 *          204:
 *              $ref: '#/components/responses/EventRoleDeleted'
 *          400:
 *              $ref: '#/components/responses/ErrorJWT'
 *          401:
 *              $ref: '#/components/responses/MissingJWT'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.delete('/', JWTMiddleware.identification, EventRoleCtrl.deleteEventRole);

module.exports = router;
