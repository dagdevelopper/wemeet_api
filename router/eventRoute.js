const EventCtrl = require('../controler/eventCtrl');
const JWTMiddleware = require('../middleware/IdentificationJWT');
const Router = require('express-promise-router');
const router = new Router;
/**
 * @swagger
 * /event/{id}:
 *  get:
 *      tags:
 *          - Event
 *      parameters:
 *          - name: id
 *            description: ID of the event
 *            in: path
 *            required: true
 *            schema:
 *              type: integer
 *      responses:
 *        200:
 *            $ref: '#/components/responses/EventFound'
 *        400:
 *            $ref: '#/components/responses/IllegalArgumentId'
 *        404:
 *            $ref: '#/components/responses/EventNotFound'
 *        500:
 *            $ref: '#/components/responses/InternalServerError'
 */
router.get('/:id', EventCtrl.getEvent);
/**
 * @swagger
 * /event/all:
 *  get:
 *      tags:
 *          - Event
 *      responses:
 *        200:
 *            $ref: '#/components/responses/EventsList'
 *        404:
 *            $ref: '#/components/responses/EventsNotFound'
 *        500:
 *            $ref: '#/components/responses/InternalServerError'
 */
router.get('/all/:email', EventCtrl.getAllEvents);
/**
 * @swagger
 * /event/{id}/participants:
 *  get:
 *      tags:
 *          - Event
 *      parameters:
 *          - name: id
 *            description: ID of the event
 *            in: path
 *            required: true
 *            schema:
 *              type: integer
 *      responses:
 *        200:
 *            $ref: '#/components/responses/ParticipantsList'
 *        400:
 *            $ref: '#/components/responses/IllegalArgumentId'
 *        404:
 *            $ref: '#/components/responses/EventNotFound'
 *        500:
 *            $ref: '#/components/responses/InternalServerError'
 */
router.get('/:id/participants', EventCtrl.getParticipants);
/**
 * @swagger
 * /event:
 *  post:
 *      tags:
 *          - Event
 *      security:
 *          - bearerAuth: []
 *      requestBody:
 *              $ref: '#/components/requestBodies/EventToCreate'
 *      responses:
 *          201:
 *              $ref: '#/components/responses/EventCreated'
 *          400:
 *              $ref: '#/components/responses/ErrorJWT'
 *          401:
 *              $ref: '#/components/responses/MissingJWT'
 *          404:
 *              $ref: '#/components/responses/UserNotFound'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.post('/', JWTMiddleware.identification, EventCtrl.createEvent);
/**
 * @swagger
 * /event:
 *  patch:
 *      tags:
 *          - Event
 *      security:
 *          - bearerAuth: []
 *      requestBody:
 *              $ref: '#/components/requestBodies/EventToUpdate'
 *      responses:
 *          201:
 *              $ref: '#/components/responses/EventUpdated'
 *          400:
 *              $ref: '#/components/responses/ErrorJWT'
 *          401:
 *              $ref: '#/components/responses/MissingJWT'
 *          404:
 *              $ref: '#/components/responses/EventNotFound'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.patch('/', JWTMiddleware.identification, EventCtrl.updateEvent);
/**
 * @swagger
 * /event:
 *  delete:
 *      tags:
 *          - Event
 *      security:
 *          - bearerAuth: []
 *      requestBody:
 *          $ref: '#/components/requestBodies/EventToDelete'
 *      responses:
 *          204:
 *               $ref: '#/components/responses/EventDeleted'
 *          400:
 *              $ref: '#/components/responses/ErrorJWT'
 *          401:
 *              $ref: '#/components/responses/MissingJWT'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.delete('/', JWTMiddleware.identification, EventCtrl.deleteEvent);

module.exports = router;
