const LikeSlCtrl = require('../controler/likedSlCtrl');
const JWTMiddleware = require('../middleware/IdentificationJWT');
const Router = require('express-promise-router');
const router = new Router;


router.post('/', JWTMiddleware.identification, LikeSlCtrl.createLike);

router.delete('/', JWTMiddleware.identification, LikeSlCtrl.deleteLike);

module.exports = router;
