const SurveyLineCtrl = require('../controler/surveyLineCtrl');
const JWTMiddleware = require('../middleware/IdentificationJWT');
const AuthLevel = require('../middleware/Authorization');
const Router = require('express-promise-router');
const router = new Router;

/**
 * @swagger
 * /surveyLine/{lineNumber}/{survey}:
 *  get:
 *      tags:
 *          - Calendar
 *      parameters:
 *          - name: line_number
 *            description: Number of the line
 *            in: path
 *            required: true
 *            schema:
 *              type: integer
 *          - name: survey
 *            description: ID of the survey
 *            in: path
 *            required: true
 *            schema:
 *              type: integer
 *      responses:
 *        204:
 *            $ref: '#/components/responses/SurveyLineFound'
 *        400:
 *            description: Bad line number or survey id.
 *        404:
 *            $ref: '#/components/responses/SurveyLineNotFound'
 *        500:
 *            $ref: '#/components/responses/InternalServerError'
 */
router.get('/:lineNumber/:survey', SurveyLineCtrl.getSurveyLine);
/**
 * @swagger
 * /surveyLine:
 *  post:
 *      tags:
 *          - SurveyLine
 *      security:
 *          - bearerAuth: []
 *      requestBody:
 *          $ref: '#/components/requestBodies/SurveyLineToCreate'
 *      responses:
 *          204:
 *              $ref: '#/components/responses/SurveyLineCreated'
 *          400:
 *              $ref: '#/components/responses/ErrorJWT'
 *          401:
 *              $ref: '#/components/responses/MissingJWT'
 *          403:
 *              $ref: '#/components/responses/MustBeAdmin'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.post('/', JWTMiddleware.identification, AuthLevel.mustBeOrganizer, SurveyLineCtrl.createSurveyLine);
/**
 * @swagger
 * /surveyLine:
 *  patch:
 *      tags:
 *          - SurveyLine
 *      security:
 *          - bearerAuth: []
 *      requestBody:
 *          $ref: '#/components/requestBodies/SurveyLineToUpdate'
 *      responses:
 *          201:
 *              $ref: '#/components/responses/SurveyLineUpdated'
 *          400:
 *              $ref: '#/components/responses/ErrorJWT'
 *          401:
 *              $ref: '#/components/responses/MissingJWT'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.patch('/', JWTMiddleware.identification, AuthLevel.mustBeOrganizer, SurveyLineCtrl.updateSurveyLine);
/**
 * @swagger
 * /surveyNine:
 *  delete:
 *      tags:
 *          - SurveyLine
 *      security:
 *          - bearerAuth: []
 *      requestBody:
 *          $ref: '#/components/requestBodies/SurveyLineToDelete'
 *      responses:
 *          204:
 *              $ref: '#/components/responses/SurveyLineDeleted'
 *          400:
 *              $ref: '#/components/responses/ErrorJWT'
 *          401:
 *              $ref: '#/components/responses/MissingJWT'
 *          403:
 *              $ref: '#/components/responses/MustBeAdmin'
 *          404:
 *              $ref: '#/components/responses/SurveyLineNotFound'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.delete('/', JWTMiddleware.identification, AuthLevel.mustBeOrganizer, SurveyLineCtrl.deleteSurveyLine);

module.exports = router;
