const RoleCtrl = require('../controler/roleCtrl');
const JWTMiddleware = require('../middleware/IdentificationJWT');
const AuthLevel = require('../middleware/Authorization');
const Router = require('express-promise-router');
const router = new Router;
/**
 * @swagger
 * /role:
 *  get:
 *      tags:
 *          - Role
 *      responses:
 *          200:
 *              $ref: '#/components/responses/RoleFound'
 *          404:
 *              $ref: '#/components/responses/RoleNotFound'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.get('/:label', RoleCtrl.getRole);
/**
 * @swagger
 * /role:
 *  post:
 *      tags:
 *          - Role
 *      security:
 *          - bearerAuth: []
 *      requestBody:
 *              $ref: '#/components/requestBodies/RoleToCreate'
 *      responses:
 *          201:
 *              $ref: '#/components/responses/RoleCreated'
 *          400:
 *              $ref: '#/components/responses/ErrorJWT'
 *          401:
 *              $ref: '#/components/responses/MissingJWT'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.post('/', JWTMiddleware.identification, AuthLevel.mustBeAdmin, RoleCtrl.createRole);
/**
 * @swagger
 * /role:
 *  patch:
 *      tags:
 *          - Role
 *      security:
 *          - bearerAuth: []
 *      requestBody:
 *              $ref: '#/components/requestBodies/RoleToUpdate'
 *      responses:
 *          201:
 *              $ref: '#/components/responses/RoleUpdated'
 *          400:
 *              $ref: '#/components/responses/ErrorJWT'
 *          401:
 *              $ref: '#/components/responses/MissingJWT'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.patch('/', JWTMiddleware.identification, AuthLevel.mustBeAdmin, RoleCtrl.updateRole);
/**
 * @swagger
 * /role:
 *  delete:
 *      tags:
 *          - Role
 *      security:
 *          - bearerAuth: []
 *      requestBody:
 *          $ref: '#/components/requestBodies/RoleToDelete'
 *      responses:
 *          204:
 *               $ref: '#/components/responses/RoleDeleted'
 *          400:
 *              $ref: '#/components/responses/ErrorJWT'
 *          401:
 *              $ref: '#/components/responses/MissingJWT'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.delete('/', JWTMiddleware.identification, AuthLevel.mustBeAdmin, RoleCtrl.deleteRole);

module.exports = router;
