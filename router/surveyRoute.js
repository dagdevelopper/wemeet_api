const SurveyCtrl = require('../controler/surveyCtrl');
const JWTMiddleware = require('../middleware/IdentificationJWT');
const AuthLevel = require('../middleware/Authorization');
const Router = require('express-promise-router');
const router = new Router;
/**
 * @swagger
 * /survey/{idEvent}:
 *  get:
 *      tags:
 *          - Survey
 *      parameters:
 *          - name: idEvent
 *            description: ID of the event
 *            in: path
 *            required: true
 *            schema:
 *              type: integer
 *      responses:
 *        200:
 *            $ref: '#/components/responses/SurveyFound'
 *        400:
 *            $ref: '#/components/responses/IllegalArgumentId'
 *        404:
 *            $ref: '#/components/responses/SurveyNotFound'
 *        500:
 *            $ref: '#/components/responses/InternalServerError'
 */
router.get('/:eventId/:userId', SurveyCtrl.getSurveysByEventId);
/**
 * @swagger
 * /survey:
 *  post:
 *      tags:
 *          - Survey
 *      security:
 *          - bearerAuth: []
 *      requestBody:
 *              $ref: '#/components/requestBodies/SurveyToCreate'
 *      responses:
 *          201:
 *              $ref: '#/components/responses/SurveyCreated'
 *          400:
 *              $ref: '#/components/responses/ErrorJWT'
 *          401:
 *              $ref: '#/components/responses/MissingJWT'
 *          403:
 *              $ref: '#/components/responses/MustBeAdmin'
 *          404:
 *              description: No survey assigned.
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 * 
 */
router.post('/', JWTMiddleware.identification, AuthLevel.mustBeOrganizer, SurveyCtrl.createSurvey);
/**
 * @swagger
 * /survey:
 *  patch:
 *      tags:
 *          - Survey
 *      security:
 *          - bearerAuth: []
 *      requestBody:
 *              $ref: '#/components/requestBodies/SurveyToUpdate'
 *      responses:
 *          201:
 *              $ref: '#/components/responses/SurveyUpdated'
 *          400:
 *              $ref: '#/components/responses/ErrorJWT'
 *          401:
 *              $ref: '#/components/responses/MissingJWT'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.patch('/', JWTMiddleware.identification, AuthLevel.mustBeOrganizer, SurveyCtrl.updateSurvey);
/**
 * @swagger
 * /survey:
 *  delete:
 *      tags:
 *          - Survey
 *      security:
 *          - bearerAuth: []
 *      requestBody:
 *          $ref: '#/components/requestBodies/SurveyToDelete'
 *      responses:
 *          201:
 *              $ref: '#/components/responses/SurveyDeleted'
 *          400:
 *              $ref: '#/components/responses/ErrorJWT'
 *          401:
 *              $ref: '#/components/responses/MissingJWT'
 *          403:
 *              $ref: '#/components/responses/MustBeAdmin'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.delete('/', JWTMiddleware.identification, AuthLevel.mustBeOrganizer, SurveyCtrl.deleteSurvey);

module.exports = router;
