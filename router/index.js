const EventRouter = require('./eventRoute');
const EventRoleRouter = require('./eventRoleRoute');
const SurveyRouter = require('./surveyRoute');
const SurveyLineRouter = require('./surveyLineRoute');
const UserRouter = require('./userRoute');
const RoleRouter = require('./roleRoute');
const CalendarRouter = require('./calendarRoute');
const LikeRouter = require('./likeSlRoute');
const InvitationRouter = require('./invitationRoute');

const router = require("express").Router();
/**
 * @swagger
 * components:
 *  responses:
 *      InternalServerError:
 *          description: Internal server error.
 *      IllegalArgumentId:
 *          description: Illegal parameter. ID must be of type 'integer'.
 */

router.use('/user', UserRouter);
router.use('/event', EventRouter);
router.use('/eventRole', EventRoleRouter);
router.use('/survey', SurveyRouter);
router.use('/surveyLine', SurveyLineRouter);
router.use('/role', RoleRouter);
router.use('/calendar', CalendarRouter);
router.use('/like', LikeRouter);
router.use('/invitation', InvitationRouter);

module.exports = router;
