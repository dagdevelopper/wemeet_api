const CalendarCtrl = require('../controler/calendarCtrl');
const Router = require('express-promise-router');
const JWTMiddleware = require('../middleware/IdentificationJWT');
const router = new Router;

/**
 * @swagger
 * /calendar/{id}/all:
 *  get:
 *      tags:
 *          - Calendar
 *      parameters:
 *          - name: id
 *            description: ID of the event
 *            in: path
 *            required: true
 *            schema:
 *              type: integer
 *      responses:
 *          200:
 *              $ref: '#/components/responses/AllCalendarsFound'
 *          400:
 *              $ref: '#/components/responses/IllegalArgumentId'
 *          404:
 *              description: No calendar found.
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.get('/:id', CalendarCtrl.getAllCalendars);
/**
 * @swagger
 * /calendar/{id}/{date}:
 *  get:
 *      tags:
 *          - Calendar
 *      parameters:
 *          - name: id
 *            description: ID of the event
 *            in: path
 *            required: true
 *            schema:
 *              type: integer
 *          - name: date
 *            description: Potential date of the event
 *            in: path
 *            required: true
 *            schema:
 *              type: string
 *      responses:
 *        200:
 *            $ref: '#/components/responses/CalendarFound'
 *        400:
 *            description: Bad event id or wrong date. Date format must be 'YYYY-MM-DD'.
 *        404:
 *            $ref: '#/components/responses/CalendarNotFound'
 *        500:
 *            $ref: '#/components/responses/InternalServerError'
 */
router.get('/:id/:date', CalendarCtrl.getCalendar);
/**
 * @swagger
 * /calendar:
 *  post:
 *      tags:
 *          - Calendar
 *      security:
 *          - bearerAuth: []
 *      requestBody:
 *          $ref: '#/components/requestBodies/CalendarToCreate'
 *      responses:
 *          201:
 *              $ref: '#/components/responses/CalendarCreated'
 *          400:
 *              $ref: '#/components/responses/ErrorJWT'
 *          401:
 *              $ref: '#/components/responses/MissingJWT'
 *          404:
 *              $ref: '#/components/responses/CalendarNotFound'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.post('/', JWTMiddleware.identification, CalendarCtrl.createCalendar);
/**
 * @swagger
 * /calendar:
 *  patch:
 *      tags:
 *          - Calendar
 *      security:
 *          - bearerAuth: []
 *      requestBody:
 *          $ref: '#/components/requestBodies/CalendarToUpdate'
 *      responses:
 *          201:
 *              $ref: '#/components/responses/CalendarUpdated'
 *          400:
 *              $ref: '#/components/responses/ErrorJWT'
 *          401:
 *              $ref: '#/components/responses/MissingJWT'
 *          404:
 *              $ref: '#/components/responses/CalendarNotFound'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.patch('/', JWTMiddleware.identification, CalendarCtrl.updateCalendar);
/**
 * @swagger
 * /calendar:
 *  delete:
 *      tags:
 *          - Calendar
 *      security:
 *          - bearerAuth: []
 *      requestBody:
 *          $ref: '#/components/requestBodies/CalendarToDelete'
 *      responses:
 *          200:
 *              $ref: '#/components/responses/CalendarDeleted'
 *          400:
 *              $ref: '#/components/responses/ErrorJWT'
 *          401:
 *              $ref: '#/components/responses/MissingJWT'
 *          404:
 *              $ref: '#/components/responses/CalendarNotFound'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.delete('/', JWTMiddleware.identification, CalendarCtrl.deleteCalendar);

module.exports = router;
