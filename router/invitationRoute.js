const InvitationCtrl = require('../controler/invitationCtrl');
const Router = require('express-promise-router');
const JWTMiddleware = require('../middleware/IdentificationJWT');
const AuthLevel = require('../middleware/Authorization');
const router = new Router;


router.get('/:email', InvitationCtrl.getInvitations);

router.get('/event/:eventId', InvitationCtrl.getInvitationsByEventId);

router.post('/', JWTMiddleware.identification, AuthLevel.mustBeOrganizer, InvitationCtrl.createInvitation);

router.patch('/', JWTMiddleware.identification, InvitationCtrl.updateInvitation);

router.delete('/', JWTMiddleware.identification, InvitationCtrl.deleteInvitation);

router.delete('/:eventId', JWTMiddleware.identification, InvitationCtrl.deleteInvitationByEvent);

module.exports = router;