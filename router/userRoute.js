const UserCtrl = require('../controler/userCtrl');
const JWTMiddleware = require('../middleware/IdentificationJWT');
const Auth = require('../middleware/Authorization');
const Router = require('express-promise-router');
const router = new Router;
/**
 * @swagger
 * /user:
 *  get:
 *      tags:
 *          - User
 *      responses:
 *          201:
 *              $ref: '#/components/responses/UserFound'
 *          400:
 *              description: Invalid email address.
 *          404:
 *              $ref: '#/components/responses/UserNotFound'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.get('/', UserCtrl.getUser);
/**
 * @swagger
 * /user/login:
 *  post:
 *      tags:
 *          - User
 *      description: Returns a JWT allowing identification.
 *      requestBody:
 *          description: User email and password in order to log in.
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#/components/schemas/Login'
 *      responses:
 *          200:
 *            description: A JSON web token
 *            content:
 *                text/plain:
 *                    schema:
 *                        type: string
 *          400:
 *            $ref: '#/components/schemas/LoginParamsError'
 *          404:
 *            $ref: '#/components/schemas/UserNotFound'
 */
router.post('/login', UserCtrl.login);
/**
 * @swagger
 * /user:
 *  post:
 *      tags:
 *          - User
 *      requestBody:
 *              $ref: '#/components/requestBodies/UserToCreate'
 *      responses:
 *          201:
 *              $ref: '#/components/responses/UserCreated'
 *          400:
 *              description: Invalid parameter(s).
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.post('/', UserCtrl.createUser);
/**
 * @swagger
 * /user:
 *  patch:
 *      tags:
 *          - User
 *      security:
 *          - bearerAuth: [] 
 *      requestBody:
 *              $ref: '#/components/requestBodies/UserToUpdate'
 *      responses:
 *          200:
 *              $ref: '#/components/responses/UserUpdated'
 *          400:
 *              description: Invalid id.
 *          404:
 *              $ref: '#/components/responses/UserNotFound'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.patch('/', JWTMiddleware.identification, UserCtrl.updateUser);
/**
 * @swagger
 * /user:
 *  patch:
 *      tags:
 *          - User
 *      security:
 *          - bearerAuth: []
 *      requestBody:
 *          $ref: '#/components/requestBodies/UserToDelete'
 *      responses:
 *          204:
 *              $ref: '#/components/responses/UserDeleted'
 *          400:
 *              description: Invalid parameter(s).
 *          404:
 *              $ref: '#/components/responses/UserNotFound'
 *          500:
 *              $ref: '#/components/responses/InternalServerError'
 */
router.delete('/', JWTMiddleware.identification, UserCtrl.deleteUser);

router.get('/all', JWTMiddleware.identification, UserCtrl.getAllUsers);

router.get('/:id', UserCtrl.getUserById);
module.exports = router;
