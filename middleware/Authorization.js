/**
 * @swagger
 * components:
 *  responses:
 *      MustBeAdmin:
 *          description: The requested action can only be made by an administrator.
 */
module.exports.mustBeAdmin = (req, res, next) => {
    
    if(req.session !== undefined && req.session.authLevel === "admin")
        next();
    else
        res.status(403).json({error: "The requested action can only be made by an administrator."});
};
/**
 * @swagger
 * components:
 *  responses:
 *      MustBeOrganizer:
 *          description: The requested action can only be made by the organizer(s).
 */
module.exports.mustBeOrganizer = (req, res, next) => {
    if(req.body !== undefined && req.body.role === "organizer")
        next();
    else
        res.status(403).json({error: "The requested action can only be made by the organizer(s)."});
};

