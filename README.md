* Pour faire fonctionner cette API, vous devez avoir VirtualBox sur votre machine

* Si vous n'avez pas encore Vagrant, bien vouloir télécharger la dernière version ici : https://www.vagrantup.com/downloads

* Pour vérifier que l'installation s'est bien passé, ouvrez un invite de commade et tapez " vagrant -v  ". Si c'est le cas, vous devriez voir la version de vagrant

FAQ
J’obtiens l’erreur suivante : «`initialize': negative string size (or size too big)» 

-> 	Supprimez le dossier « C:/Users/VOTRE_USER/.vagrant.d » 

J’obtiens l’erreur suivante : « Error: SSL certificate problem: self signed certificate in certificate chain » (généralement pour ceux ayant Kaspersky comme antivirus)
->	Utilisez la commande : « vagrant box add --insecure ubuntu/bionic64 »


---------------------------------------------
* Dans l'invite de Commande, à la racine du projet, faites " vagrant up "

* pour initialiser le projet faites " npm i"

* pour se rassurer que tout fonctionne bien du coté de la base de donnée : tapez la commande : " npm run initDB"

* puis pour lancer le projet, faites : "npm run dev"
