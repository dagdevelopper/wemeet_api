const Utils = require("../utils/utils");

module.exports.createUser = async (client, {address, email, firstName, isAdmin, lastName, password, postalCode}) => {
    return await client.query(`
        INSERT INTO "user"(first_name, last_name, email, password, address, postal_code, is_admin)
        VALUES ($1, $2, $3, $4, $5, $6, $7)`,
        [firstName, lastName, email, await Utils.getHash(password), address, postalCode, isAdmin]
    );
};

module.exports.getAllUsers = async (client) => {
    return await client.query(`SELECT * FROM "user"`);
}

module.exports.getUser = async (client, email) => {
    return await client.query(`SELECT * FROM "user" WHERE email = $1`, [email]);
};

module.exports.getUserById = async (client, id) => {
    return await client.query(`SELECT * FROM "user" WHERE id = $1`, [id]); 
};

module.exports.getUsersByEvent = async (client, eventId) => {
    return await client.query(`
        SELECT er.role AS role, u.* 
        FROM event_role er
        JOIN "user" u
            ON er."user" = u.id
        JOIN event e
            ON er.event = e.id
        WHERE e.id = $1
        ORDER BY er.role`,
        [eventId]
    );
}

module.exports.updateUser = async (client, {id, firstName, lastName, email, password, address, postalCode, isAdmin}) => {
    const params = [];
    const querySet = [];

    let query = 'UPDATE "user" SET ';
    if(firstName !== undefined){
        params.push(firstName);
        querySet.push(` first_name = $${params.length} `);
    }
    if(lastName !== undefined){
        params.push(lastName);
        querySet.push(` last_name = $${params.length} `); 
    }
    if(email !== undefined){
        params.push(email);
        querySet.push(` email = $${params.length} `);
    }
    if(password !== undefined){
        params.push(await Utils.getHash(password));
        querySet.push(` password = $${params.length} `);
    }
    if(address !== undefined){
        params.push(address);
        querySet.push(` address = $${params.length} `);
    }
    if(postalCode !== undefined){
        params.push(postalCode);
        querySet.push(` postal_code = $${params.length} `);
        
    }
    if(isAdmin !== undefined){
        params.push(isAdmin);
        querySet.push(` is_admin = $${params.length} `);
    }

    query += querySet.join(',');
    params.push(id);
    query += `WHERE id=$${params.length}`;
    return await client.query(query, params);
};

module.exports.deleteUser = async (client, id, email) => {
    const params = [];
    const querySet = [];

    let query = 'DELETE FROM "user" WHERE';
    if(id !== undefined) {
        params.push(id);
        querySet.push(` id = $${params.length} `);
    }
    if(email !== undefined){
        params.push(email);
        querySet.push(` email = $${params.length}`);
    }

    query += querySet.join('AND');
    return await client.query(query, params);
}
