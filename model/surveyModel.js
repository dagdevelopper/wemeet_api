// module.exports.getSurveysByEventId = async (client, event_id) => {
//     return await client.query(`
//         SELECT s.title, s.id, s.description, sl.label, sl.line_number, sl.nb_votes FROM survey s JOIN survey_line sl ON s.id = sl.survey WHERE event = $1;`, [event_id]
//     );
// }

module.exports.getSurveysByEventId = async (client, event_id) => {
    return await client.query(`
        SELECT * from survey WHERE event = $1;`, [event_id]
    );
}

module.exports.createSurvey = async (client, id_event, label, description) => {
    return await client.query (`
        INSERT INTO survey (title, description, event) VALUES ($1, $2, $3) RETURNING id`, [label, description, id_event]
    );
}

module.exports.updateSurvey = async (client, id, label, description) => {

    const params = [];
    const querySet = [];
    let query = "UPDATE survey SET";

    if (label !== undefined){
        params.push(label);
        querySet.push(` title = $${params.length} `);
    }
    if (description !== undefined){
        params.push(description);
        querySet.push(` description = $${params.length} `);
    }
    
    if (params.length > 0){
        query += querySet.join(',');
        params.push(id);
        query += ` WHERE id = $${params.length}`;
        return client.query(query, params);
    }else
        throw new Error("No field to update");
}

module.exports.deleteSurvey = async (client, id) => {
    return await client.query(`
        DELETE FROM survey WHERE id = $1`, [id]
    );
}

module.exports.surveyExist = async (client, survey) => { 
    const {rows} = await client.query (
        "SELECT count(id) AS nbr FROM survey WHERE id = $1", [survey]
    );

    return rows[0].nbr > 0;
}
