module.exports.createCalendar = async (client, event_id, date, nb_yes = 0, nb_no = 0, nb_maybe = 0) => {
    return await client.query (
        `INSERT INTO calendar (event, date, nb_yes, nb_no, nb_maybe) VALUES ($1, to_date($2, 'YYYY-MM-DD'), $3, $4, $5)`
        , [event_id, date, nb_yes, nb_no, nb_maybe]
    );
};

module.exports.deleteCalendar = async (client, event_id, date) => {
    return await client.query(
        `DELETE FROM calendar WHERE event = $1 AND date = $2`, [event_id, date]
    );
};

module.exports.updateCalendar = async (client, event_id, old_date, new_date) => {
    if (new_date === undefined)
        throw new Error("No field to update");
    
    return await client.query(
        `UPDATE calendar SET date = $1 WHERE event = $2 AND date = $3`, [new_date, event_id, old_date]
    );
};

module.exports.getCalendar = async (client, event_id, date) => {
    return await client.query (
        `SELECT * FROM calendar WHERE event = $1 AND date = $2`, [event_id, date]
    );
};

module.exports.calendarExist = async (client, event_id, date) => {
    const {rows} = await client.query(`SELECT COUNT(event) AS nb FROM calendar WHERE event = $1 AND date = $2`, [event_id, date]);
    return rows[0].nb > 0;
};

module.exports.getAllCalendars = async (client, event) => {
    return await client.query(`SELECT * FROM calendar WHERE event = $1`, [event]);
};
