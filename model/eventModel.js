module.exports.createEvent = async (client, label, description, cityName, isPrivate, dateBegin, dateEnd, creation_date) => {
    return await client.query(`
        INSERT INTO event(label, description, city_name, is_private, date_begin, date_end, creation_date) VALUES
        ($1, $2, $3, $4, $5, $6, $7) RETURNING id`, [label, description, cityName, isPrivate, dateBegin, dateEnd, creation_date]
    );
}

module.exports.getEvent = async (client, id) => {
    return await client.query("SELECT * FROM event WHERE id=$1", [id]);
}

module.exports.getAllEvent = async (client) => {
    return await client.query("SELECT * FROM event");
}

module.exports.updateEvent = async (client, id, label, description, cityName, isPrivate, dateBegin, dateEnd) => {

    const params = [];
    const querySet = [];
    let query = "UPDATE event SET";

    if (label !== undefined) {
        params.push(label);
        querySet.push(` label = $${params.length} `);
    }
    if (description !== undefined) {
        params.push(description);
        querySet.push(` description = $${params.length} `);
    }
    if (cityName !== undefined) {

        params.push(cityName);
        querySet.push(` city_name = $${params.length} `);
    }
    if (isPrivate !== undefined) {
        params.push(isPrivate);
        querySet.push(` is_private = $${params.length} `);
    }
    if (dateBegin !== undefined) {
        params.push(dateBegin);
        querySet.push(` date_begin = $${params.length}`);
    }
    if (dateEnd !== undefined) {
        params.push(dateEnd);
        querySet.push(` date_end = $${params.length}`);
    }


    if (params.length > 0) {
        query += querySet.join(',');
        params.push(id);
        query += ` WHERE id = $${params.length}`;
        return client.query(query, params);
    } else
        throw new Error("No field to update");
}

module.exports.deleteEvent = async (client, id) => {
    return await client.query("DELETE FROM event WHERE id=$1", [id]);
}

module.exports.eventExist = async (client, id) => {
    const { rows } = await client.query(`SELECT COUNT(id) AS nb FROM event WHERE id = $1`, [id]);
    return rows[0].nb > 0;
}
