module.exports.createEventRole = async (client, user, eventId, role) => {
    return await client.query(`INSERT INTO event_role ("user", event, role) VALUES ($1, $2, $3)`, [user, eventId, role]);
}

module.exports.getEventRole = async (client, user, eventId) => {
    return await client.query(`SELECT * FROM event_role WHERE "user" = $1 AND event = $2`, [user, eventId]);
}

//new Function
module.exports.getEventRoleByUser = async (client, userId) => {
    return await client.query(`SELECT * FROM event_role WHERE "user" = $1`, [userId]);
}

module.exports.getEventRoles = async (client, eventId) => {
    return await client.query(`SELECT * FROM event_role WHERE event = $1`, [eventId]);
}

module.exports.updateEventRole = async (client, user, eventId, role) => {
    return await client.query(`UPDATE event_role SET role = $1 WHERE "user" = $2 AND event = $3`, [user, eventId, role]);
}

module.exports.deleteEventRole = async (client, user, eventId) => {
    return await client.query(`DELETE FROM event_role WHERE "user" = $1 AND event = $2`, [user, eventId]);
}

module.exports.eventRoleExists = async ({client, user, event}) => {
    const {rows} =  await client.query(`SELECT COUNT(event) AS nb FROM event_role WHERE "user" = $1 AND event = $2`, [user, event]);
    return rows[0].nb > 0;
}
