

module.exports.getInvitations = async ({client, email}) => {
    return await client.query(`SELECT * FROM invitation WHERE guest = $1`, [email]);
}

module.exports.getInvitationsByEventId = async ({client, eventId}) => {
    return await client.query(`SELECT * FROM invitation WHERE event = $1`, [eventId]);
}

module.exports.createInvitation = async ({client, userId, eventId, guest, accepted = false}) => {
    return await client.query(`INSERT INTO invitation (sender, guest, event, accepted) VALUES ($1, $2, $3, $4)`, [userId, guest, eventId, accepted]);
}

module.exports.deleteInvitation = async ({client, guest, eventId}) => {
    return await client.query(`DELETE FROM invitation WHERE guest = $1 AND event = $2`, [guest, eventId]);
}

module.exports.deleteInvitationByEvent = async ({client, eventId}) => {
    return await client.query(`DELETE FROM invitation WHERE event = $1`, [eventId]);
}

module.exports.invitationExists = async ({client, guest, eventId}) => {
    const { rows } = await client.query(`SELECT COUNT(guest) AS nb FROM invitation WHERE event = $1 AND guest = $2`, [eventId, guest]);
    return rows[0].nb > 0;
}

module.exports.invitationsExists = async ({client, eventId}) => {
    const { rows } = await client.query(`SELECT COUNT(event) AS nb FROM invitation WHERE event = $1`, [eventId]);
    return rows[0].nb > 0;
}

module.exports.invitationAcceptedExists = async ({client, guest, eventId}) => {
    const { rows } = await client.query(`SELECT COUNT(guest) AS nb FROM invitation WHERE event = $1 AND guest = $2 AND accepted = $3`, [eventId, guest, true]);
    return rows[0].nb > 0;
}

module.exports.updateInvitation = async ({client, guest, eventId, accepted}) => {
    return await client.query(`UPDATE invitation SET accepted = $1 WHERE guest = $2 AND event = $3`, [accepted, guest, eventId]);
}

