module.exports.deleteSurveyLine = async (client, lineNumber, surveyId) => {

    return await client.query (
        `DELETE FROM survey_line WHERE line_number = $1 AND survey = $2`, [lineNumber, surveyId]
    );
}

module.exports.createSurveyLine = async (client, survey, label, nbVotes, liked) => {
    return await client.query(
        `INSERT INTO survey_line (survey, label, nb_votes, liked) VALUES ($1, $2, $3, $4) RETURNING line_number`, [survey, label, nbVotes, liked]
    );
}


module.exports.updateAllSurveyLine = async (client, survey, liked = false) => {
    return await client.query(`UPDATE survey_line SET liked = $1 WHERE survey = $2`, [liked, survey]);
}

module.exports.updateSurveyLine = async (client, label, nbVotes, survey, lineNumber, liked) => {

    const params = [];
    const querySet = [];
    let query = "UPDATE survey_line SET";

    if (label !== undefined){
        params.push(label);
        querySet.push(` label = $${params.length} `);
    }

    if (nbVotes !== undefined){
        params.push(nbVotes);
        querySet.push(` nb_votes = $${params.length} `);
    }

    if (liked !== undefined){
        params.push(liked);
        querySet.push(` liked = $${params.length} `);
    }

    if (params.length > 0){
        query += querySet.join(',');
        params.push(survey);
        query += ` WHERE survey = $${params.length}`;
        params.push(lineNumber);
        query += ` AND line_number = $${params.length}`;
        return client.query(query, params);
    }else
        throw new Error("No field to update");
}

module.exports.getSurveyLine = async (client, line_number, survey) => {
    return await client.query (
        `SELECT * FROM survey_line WHERE line_number = $1 AND survey = $2`, [line_number, survey]
    );
}

module.exports.getSurveyLineBySurvey = async ({client, survey}) => {
    return await client.query (
        `SELECT * FROM survey_line WHERE survey = $1`, [survey]
    );
}

module.exports.surveyLineExist = async (client, line_number, survey) => {
    const {rows} = await client.query(`SELECT COUNT(line_number) AS nb FROM survey_line WHERE line_number = $1 AND survey = $2`, [line_number, survey]);
    return rows[0].nb > 0;
}
