module.exports.createRole = async (client, label) => {
    return await client.query("INSERT INTO role (label) VALUES ($1)", [label]);
};

module.exports.getRole = async(client, label) => {
    return await client.query("SELECT * FROM role WHERE label = $1", [label]);
};

module.exports.updateRole = async (client, oldLabel, newLabel) => {
    return client.query("UPDATE role SET label = $1 WHERE label = $2", [newLabel, oldLabel]);
}

module.exports.deleteRole = async (client, label) => {
    return client.query("DELETE FROM role WHERE label = $1", [label]);
}
