

module.exports.getLikes = async ({client, survey_line, person}) => {
    return await client.query (
        `SELECT * FROM liked_sl WHERE survey_line = $1 AND person = $2`, [survey_line, person]
    );
}

module.exports.getAllLikes = async ({client, survey_line}) => {
    return await client.query (
        `SELECT * FROM liked_sl WHERE survey_line = $1`, [survey_line]
    );
}


module.exports.deleteLike = async ({client, survey_line, person}) => {

    return await client.query (
        `DELETE FROM liked_sl WHERE survey_line = $1 AND person = $2`, [survey_line, person]
    );
}

module.exports.createLike = async ({client, survey_line, person}) => {
    return await client.query(
        `INSERT INTO liked_sl (survey_line, person) VALUES ($1, $2)`, [survey_line, person]
    );
}

/**
 * (1, 1, 1),
(1, 2, 1),
(2, 4, 1),
(1, 1, 2),
(2, 4, 2),
(1, 1, 3),
(1, 2, 3),
(2, 6, 4),
(1, 2, 4),
(2, 4, 4),
(1, 1, 5),
(1, 3, 5),
(1, 2, 5),
(1, 1, 6),
(1, 1, 7),
(1, 2, 7),
(1, 3, 7),
(4, 9, 8);
 */
