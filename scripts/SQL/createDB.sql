DROP TABLE IF EXISTS event CASCADE;
DROP TABLE IF EXISTS survey CASCADE;
DROP TABLE IF EXISTS survey_line CASCADE;
DROP TABLE IF EXISTS calendar CASCADE;
DROP TABLE IF EXISTS "user" CASCADE;
DROP TABLE IF EXISTS role CASCADE;
DROP TABLE IF EXISTS event_role CASCADE;
DROP TABLE IF EXISTS liked_sl CASCADE;
DROP TABLE IF EXISTS invitation CASCADE;

CREATE TABLE event (
    id          INT     GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    label       TEXT    NOT NULL,
    description TEXT,
    city_name   TEXT    NOT NULL,
    is_private  BOOLEAN NOT NULL,
    date_begin DATE NOT NULL,
    date_end DATE,
    creation_date NUMERIC(13, 0) NOT NULL
);

CREATE TABLE survey (
    id          INT     GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    title       TEXT    NOT NULL,
    description TEXT,
    event       INT     REFERENCES event(id) ON DELETE SET NULL
);

CREATE TABLE survey_line (
    survey      INT     REFERENCES survey(id) ON DELETE CASCADE,
    line_number INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    label       TEXT    NOT NULL,
    nb_votes    INT     NOT NULL,
    liked       BOOLEAN NOT NULL
);

CREATE TABLE calendar (
    event       INT     REFERENCES event(id) ON DELETE CASCADE,
    "date"      DATE,
    nb_yes      INT     NOT NULL,
    nb_no       INT     NOT NULL,
    nb_maybe    INT     NOT NULL,
    PRIMARY KEY (event, "date")
);


CREATE TABLE "user" (
    id          INT     GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    first_name  TEXT    NOT NULL,
    last_name   TEXT    NOT NULL,
    email       TEXT    UNIQUE NOT NULL,
    password    TEXT    NOT NULL,
    address     TEXT,
    postal_code INT,
    is_admin    BOOLEAN NOT NULL
);

CREATE TABLE liked_sl (
    survey_line   INT,
    person        INT,
    FOREIGN KEY (survey_line) REFERENCES survey_line(line_number) ON DELETE CASCADE,
    FOREIGN KEY (person)      REFERENCES "user"(id) ON DELETE CASCADE,
    PRIMARY KEY (survey_line, person)
);

CREATE TABLE role (
    label TEXT PRIMARY KEY
);

CREATE TABLE event_role (
    "user"  INT  REFERENCES "user"(id) ON DELETE CASCADE,
    event   INT  REFERENCES event(id) ON DELETE CASCADE,
    role    TEXT REFERENCES role(label) ON DELETE CASCADE,
    PRIMARY KEY ("user", event)
);

CREATE TABLE invitation (
    sender  INT  NOT NULL,
    event   INT  REFERENCES event(id) ON DELETE CASCADE,
    accepted BOOLEAN NOT NULL,
    guest TEXT REFERENCES "user"(email) ON DELETE CASCADE,
    PRIMARY KEY (guest, event)
);


INSERT INTO event (label, description, city_name, is_private, date_begin, date_end, creation_date) VALUES
('fete de wallonie', 'vient on va samuser comme des fous à la fete de wallonie !', 'namur', false, TO_DATE('2020-11-13','YYYY-MM-DD'), TO_DATE('2020-11-14','YYYY-MM-DD'), 1600905600000),
('on sort ?', 'on ira bire des bière. je vous invite !', 'bruxelles', false, TO_DATE('2021-04-02','YYYY-MM-DD'), TO_DATE('2021-04-02','YYYY-MM-DD'), 1615852800000),
('foire de liège', 'vient on va samuser comme des fous à la foire de liège !', 'liège', false, TO_DATE('2023-01-24','YYYY-MM-DD'), TO_DATE('2023-01-24','YYYY-MM-DD'), 1645660800000),
('concert de tayc', 'on que des filles, il nous faut de la compagnie !', 'anvers', false, TO_DATE('2022-12-30','YYYY-MM-DD'), TO_DATE('2022-12-31','YYYY-MM-DD'), 1669852800000),
('petit bitu', 'go faire la fête jusque tard le soir !', 'namur', false, TO_DATE('2022-12-07','YYYY-MM-DD'), TO_DATE('2022-12-08','YYYY-MM-DD'), 1669852800000),
('soirée de gala', 'un pote qui connait dadju nous invite.', 'bruxelles', true, TO_DATE('2023-12-09','YYYY-MM-DD'), TO_DATE('2022-12-09','YYYY-MM-DD'), 1669852800000),
('soirée ciné', 'le nouveau cinéma qui a ouvert!', 'liège', true, TO_DATE('2023-01-05','YYYY-MM-DD'), TO_DATE('2023-01-05','YYYY-MM-DD'), 1670284800000),
('manif contre le corona', 'on ne doit pas obliger les enfants à se faire vaciner !', 'louvain', false, TO_DATE('2022-12-05','YYYY-MM-DD'), TO_DATE('2022-12-05','YYYY-MM-DD'), 1670198400000),
('sortie entre amis', 'vient on va samuser comme des fous à la foire de liège !', 'mons', false, TO_DATE('2023-12-24','YYYY-MM-DD'), TO_DATE('2023-12-25','YYYY-MM-DD'), 1670457600000),
('anniv de lucie', 'cest un anniversaire surprise.. ramenez des cadeaux !', 'charleroi', true, TO_DATE('2022-12-25','YYYY-MM-DD'), TO_DATE('2022-12-25','YYYY-MM-DD'), 1670284800000);

INSERT INTO survey (title, description, event) VALUES
('thème en blanc', 'pas une autre couleur que le blanc svp !', 2),
('Moyen de transport', 'si vous avez une voiture, vous pouvez proposer', 1),
('Chacun apporte à manger', '', 3),
('Vêtement en commun', 'Pour se reconaitre plus facilement', 1);

INSERT INTO survey_line (survey, label, nb_votes, liked) VALUES
(1,'Blanc cassé', 2, false),
(1,'Blanc coco', 5, true),
(1,'Blanc neige', 3, false),
(2,'On prend le bus', 5, true),
(2,'voiture', 6, false),
(2,'en van', 4, false),
(3,'Oui', 2, false),
(3,'Non', 1, false),
(4,'Non', 7, false),
(4,'Oui', 4, false);



INSERT INTO "user" (first_name, last_name, email, password, address, postal_code, is_admin) VALUES
('Vially', 'Armel', 'vially@mail.com', '$2a$10$0zeMnTcmdyiMeJkfPi.CJuCdmVAdMKy/IwE24MrdLmbUpl5CwYTx6', 'Rue quelque part', 5000, true), /* mdp : 1234 */
('Pierre-Olivier', 'Manil', 'po@mail.com', '$2a$10$0zeMnTcmdyiMeJkfPi.CJuCdmVAdMKy/IwE24MrdLmbUpl5CwYTx6', 'Rue autre part', 5000, true),/* mdp : 1234 */
('Albus', 'Dumbledore', 'ad@mail.com', '$2a$10$7YKDcUsfRgULkGeepDY8feNSaqNcy6E7jWpBoYnB6qsD5CaBebHBG', 'Poudlard', 123455, false),/* mdp : Ab22 */
('Bruce', 'Wayne', 'bw@mail.com', '$2a$10$ykV3siDr3gZHNzfZ7oO/Ge9Ae9AwQxLwC46bLMOXNFnVuH5JkEGVm', 'Gotham City', 987411, false),/* mdp : BW */
('Minerva', 'McGonagall', 'minerva_mg@mail.com', '$2a$10$.NOOhIQxPf/5VMTVouCe3u/tKYEa2rSIO4z55gkdo1Uu5Skp3pdpu', 'Poudlard', 123455, false),/* mdp : l9aEz0 */
('Sakura', 'Haruno', 'saha@mail.com', '$2a$10$D3M2nstpaQ9qGVF2m/pEge6BLGDHZq8WXGIgQnNk81PJOU6rgbRdC', 'Konoha', 5768741, false),/* mdp : M9BEg5 */
('Maggie', 'Simpson', 'smag@mail.com', '$2a$10$4tbWHWVL79xot7Ao4.06PeyiWX4gMXw8XD.dP7EBClPjrA1p/Zslm', 'Evergreen Terrace, Springfield', 55489, false),/* mdp : feministPower */
('Karui', 'Akimichi', 'karui_Aki@mail.com', '$2a$10$yBeInAyoBoeu/5CaCwdc1OoCOAQf7488rW1MrrNn7YgSjV11mAXBa', 'Kumo', 757798, false);/* mdp : lala12 */

INSERT INTO invitation (sender, guest, event, accepted) VALUES
(4, 'vially@mail.com', 6, false),
(7, 'vially@mail.com', 9, false);

INSERT INTO liked_sl (survey_line, person) VALUES
(1, 1),
(2, 1),
(4, 1),
(1, 2),
(4, 2),
(1, 3),
(2, 3),
(1, 4),
(2, 4),
(4, 4),
(1, 5),
(3, 5),
(2, 5),
(1, 6),
(1, 7),
(2, 7),
(3, 7),
(1, 8);

INSERT INTO role (label) VALUES 
('organizer'), ('participant'), ('test');


INSERT INTO event_role  ("user", event, role)VALUES
(1, 1, 'organizer'),
(2, 3, 'organizer'),
(2, 2, 'organizer'),
(1, 4, 'organizer'),
(3, 5, 'organizer'),
(4, 6, 'organizer'),
(5, 7, 'organizer'),
(6, 8, 'organizer'),
(7, 9, 'organizer'),
(8, 10, 'organizer'),

(3, 2, 'participant'),
(4, 3, 'participant'),
(4, 10, 'participant'),
(5, 4, 'participant'),
(6, 5, 'participant'),
(7, 6, 'participant'),
(8, 7, 'participant'),
(2, 10, 'participant'),
(3, 10, 'participant'),
(3, 1, 'participant'),
(4, 1, 'participant'),
(5, 1, 'participant'),
(6, 1, 'participant'),
(7, 1, 'participant'),
(8, 1, 'participant');

INSERT INTO calendar (event, date, nb_yes, nb_no, nb_maybe)
VALUES
    (1,TO_DATE('2021-12-24','YYYY-MM-DD'),1,3,2),
    (1,TO_DATE('2022-01-07','YYYY-MM-DD'),4,2,1),
    (2,TO_DATE('2022-01-11','YYYY-MM-DD'),1,0,7),
    (2,TO_DATE('2022-01-16','YYYY-MM-DD'),1,1,5),
    (3,TO_DATE('2022-01-22','YYYY-MM-DD'),1,6,0);
